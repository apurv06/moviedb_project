package com.example.apurv.flick.Services;

import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.CastAndCrew.CastCrew;
import com.example.apurv.flick.Fragments.Movie.OmdbInfo.Omdbinfo;
import com.example.apurv.flick.Images.ImagesRoot;
import com.example.apurv.flick.MoviesDirectory.Genre.GenreMovie;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.MoviesDirectory.Video_Path_API;
import com.example.apurv.flick.MoviesDirectory.movie;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MoviesService {

    @GET("{category}?api_key="+ ApiHolder.movieDbApi+"&language=en-US")
    Call<Movies_rootOfAPI> getMovieList(@Path("category")String cat, @Query("page") String page);

    @GET("{movie_id}/videos?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<Video_Path_API> getTrailerPath(@Path("movie_id")String movieId);

    @GET("{movie_id}/credits?api_key="+ApiHolder.movieDbApi+"")
    Call<CastCrew> getCastAndCrew(@Path("movie_id")String movieId);

    @GET("{movie_id}/{type}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<Movies_rootOfAPI> getSuggestedMovies(@Path("movie_id")String movieId,@Path("type")String type);

    @GET("{movie_id}/images?api_key="+ApiHolder.movieDbApi+"&language=en-US&include_image_language=null")
    Call<ImagesRoot> getImagesPath(@Path("movie_id")String s);

    @GET("list?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<GenreMovie> getMovieGenre();

    @GET("?apikey="+ApiHolder.omdbDbApi)
    Call<Omdbinfo> getInofromOmdb(@Query("i") String imdb_id);

    @GET("{movie_id}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<movie> getMovie(@Path("movie_id")String movieId);


}
