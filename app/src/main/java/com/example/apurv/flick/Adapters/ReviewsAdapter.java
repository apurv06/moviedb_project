package com.example.apurv.flick.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.apurv.flick.R;
import com.example.apurv.flick.TvDirectory.Result;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder > {

    ArrayList<Result> list;

    public ReviewsAdapter(ArrayList<Result> list) {
        this.list=list;
    }

    @NonNull


    public ReviewsAdapter.ReviewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
      view=inflater.inflate(R.layout.review_layout,parent,false);
        final ReviewsAdapter.ReviewsViewHolder mViewHolder=new ReviewsAdapter.ReviewsViewHolder(view);
        
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsViewHolder holder, int position) {

        String as=list.get(position).getAuthor();
        holder.author.setText(list.get(position).getAuthor());
        holder.content.setText(list.get(position).getContent());

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ReviewsViewHolder extends RecyclerView.ViewHolder{
        TextView author;
        ExpandableTextView content;
        public ReviewsViewHolder(@NonNull View itemView) {
            super(itemView);
            author=itemView.findViewById(R.id.author);
            content= (ExpandableTextView) itemView.findViewById(R.id.sample1).findViewById(R.id.expand_text_view);
        }
    }
}
