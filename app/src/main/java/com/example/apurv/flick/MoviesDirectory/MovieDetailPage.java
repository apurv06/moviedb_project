package com.example.apurv.flick.MoviesDirectory;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.ImagesVideosAdapter;
import com.example.apurv.flick.Database.AppDatabase;
import com.example.apurv.flick.Database.Movie;
import com.example.apurv.flick.Database.MovieDao;
import com.example.apurv.flick.Fragments.Movie.CastFragment;
import com.example.apurv.flick.Fragments.ImageViewPagerFragment;
import com.example.apurv.flick.Fragments.Movie.InfoFragment;
import com.example.apurv.flick.Fragments.Movie.MovieDetailsPageFragment;
import com.example.apurv.flick.Fragments.Movie.ReviewsFragment;
import com.example.apurv.flick.HomeActivity;
import com.example.apurv.flick.ImageAndVideo;
import com.example.apurv.flick.Images.ImagesRoot;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.Settings;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDetailPage extends AppCompatActivity implements View.OnClickListener,InfoFragment.OnFragmentInteractionListener,CastFragment.OnFragmentInteractionListener,ReviewsFragment.OnFragmentInteractionListener,ImageViewPagerFragment.OnFragmentInteractionListener{

    ArrayList<ImageAndVideo> movievideos;
    ArrayList<ImageAndVideo> movieimages;
    ArrayList<ImageAndVideo> arr;
    android.support.v7.widget.Toolbar toolbar;
    ImagesVideosAdapter imagesVideosAdapter;
    AppBarLayout appBarLayout;
    YouTubePlayerFragment myYouTubePlayerFragment;
    FrameLayout frameLayout;
    String video_key;
    Bundle bundle;
    movie obj;
    RadioGroup radioGroup;
    ViewPager viewPager;

    CheckBox checkBox;

    int ImagesPosition;
    Boolean flag=false;

    RadioButton videosButton;
    RadioButton imagesButton;

    AppDatabase mDb;
    MovieDao mMovieDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail_page);


         Intent intent = getIntent();
         bundle = intent.getExtras();
         obj=new Gson().fromJson(bundle.getString("movieObject"),movie.class);
        checkBox=findViewById(R.id.favourite);
            movievideos=new ArrayList<>();
            movieimages=new ArrayList<>();
            arr=new ArrayList<>();
            loadTrailer();
        mDb = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"database_1").allowMainThreadQueries().build();
        mMovieDao = mDb.MovieDao();// Get DAO object
        Movie obj1=mMovieDao.Search(obj.getId());
        if (obj1!=null)
        {
            checkBox.setChecked(true);
        }


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    AppDatabase mDb = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"database_1").allowMainThreadQueries().build();
                    MovieDao mMovieDao = mDb.MovieDao();// Get DAO object
                    Movie movie = new Movie(obj.getId());

                    mMovieDao.insert(movie); // Insert it in database
                    Toast.makeText(MovieDetailPage.this, "Added To Favourites", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(MovieDetailPage.this, "Removed From Favourites", Toast.LENGTH_SHORT).show();
                    Movie obj1=mMovieDao.Search(obj.getId());
                    mMovieDao.delete(obj1);
                }
            }
        });

//        toolbar=findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        MovieDetailsPageFragment fragment = new MovieDetailsPageFragment();
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.add(R.id.contentFrame,fragment);
        transaction.commit();

        viewPager=findViewById(R.id.viewPager);
        imagesVideosAdapter =new ImagesVideosAdapter(getSupportFragmentManager(),arr,MovieDetailPage.this);
        viewPager.setAdapter(imagesVideosAdapter);

        videosButton=findViewById(R.id.Videos);
        imagesButton=findViewById(R.id.images);

        if(Settings.default_Header_details_page == video.Type)
        {
            videosButton.setChecked(true);
        }
        else
        {
            imagesButton.setChecked(true);
        }

        final ViewPager.OnPageChangeListener pagerchangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                imagesVideosAdapter.onPageChanged(position);
                if(arr.get(position).getType()==video.Type)
                {
                    videosButton.setChecked(true);
                    flag=true;
                }
                else
                {
                    imagesButton.setChecked(true);
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        viewPager.addOnPageChangeListener(pagerchangeListener);


        radioGroup=findViewById(R.id.radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (flag) {
                    flag=false;
                    if (i == R.id.Videos) {
                        viewPager.setCurrentItem(0, true);

                    } else {
                        viewPager.setCurrentItem(ImagesPosition, true);
                    }
                }
                else
                {
                    if (i == R.id.Videos) {
                        viewPager.setCurrentItem(movievideos.size()-1, true);

                    } else {
                        viewPager.setCurrentItem(ImagesPosition, true);
                    }
                }
            }
        });


    }



    private void loadTrailer() {

        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

       Retrofit retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<Video_Path_API> call= service.getTrailerPath(obj.getId()+"");
        call.enqueue(new Callback<Video_Path_API>() {
            @Override
            public void onResponse(Call<Video_Path_API> call, Response<Video_Path_API> response) {
                Video_Path_API r=response.body();
                ArrayList<video> downloadedVideosPaths=r.getResults();
                movievideos.addAll(downloadedVideosPaths);
               arr.addAll(movievideos);
                loadimages();
                imagesVideosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Video_Path_API> call, Throwable t) {

            }
        });

    }
    private void loadimages() {

        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<ImagesRoot> call= service.getImagesPath(obj.getId()+"");
        call.enqueue(new Callback<ImagesRoot>() {
            @Override
            public void onResponse(Call<ImagesRoot> call, Response<ImagesRoot> response) {
                ImagesRoot r=response.body();
                ImagesPosition=arr.size();
                movieimages.addAll(r.getBackdrops());
                movieimages.addAll(r.getPosters());

                arr.addAll(movieimages);
                imagesVideosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ImagesRoot> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(View view) {


    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public void GoHome(View view) {
        Intent intent=new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}
