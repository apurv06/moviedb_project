package com.example.apurv.flick.MoviesDirectory;

import com.example.apurv.flick.Adapters.MediaType;
import com.example.apurv.flick.MoviesDirectory.Genre.Genre;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class movie implements Serializable,MediaType {



    int vote_count;
    int id;
    boolean video;
    public double vote_average;
    String title;
    Boolean adult;
    ArrayList<video> videos;

    public movie() {
        videos=new ArrayList<>();
    }

    public ArrayList<com.example.apurv.flick.MoviesDirectory.video> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<com.example.apurv.flick.MoviesDirectory.video> videos) {
        this.videos.addAll(videos);
    }

    public double getVote_average() {
        return vote_average;
    }

    Double popularity;
    String poster_path;
    String overview;
    String release_date;
    String original_language;
    String original_title;
    Long[]  genre_ids;
    String backdrop_path;

    public int getVote_count() {
        return vote_count;
    }

    public int getId() {
        return id;
    }
    public boolean isVideo() {
        return video;
    }
    public Long[] getGenre_ids() {
        return genre_ids;
    }



    public String getRelease_date() {
        return release_date;
    }

    public String getOverview() {
        return overview;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public Double getPopularity() {
        return popularity;
    }


    public Boolean getAdult() {
        return adult;
    }


    public String getTitle() {
        return title;
    }
    public String getOriginal_title() {
        return original_title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    HashMap<Long,String> map;
ArrayList<String> Genres;
//ArrayList<String> AllGenre;
    public ArrayList<String> getGenres() {
        return Genres;
    }

    public void setGenres(ArrayList<Genre> mGenres) {
        Genres=new ArrayList<>();
        map=new HashMap<>();
        for (int i=0;i<mGenres.size();i++)
        {
            map.put(mGenres.get(i).getId(),mGenres.get(i).getName());
        }

        for (int i=0;i<genre_ids.length;i++)
        {

                Genres.add(map.get(genre_ids[i]));

        }

    }

    @Override
    public int getType() {
        return 1;
    }
}
