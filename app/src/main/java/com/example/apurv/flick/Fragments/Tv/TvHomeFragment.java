package com.example.apurv.flick.Fragments.Tv;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.card.MaterialCardView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.Adapters.TvAdapter;
import com.example.apurv.flick.Adapters.ViewPagerTvAdapter;
import com.example.apurv.flick.Animations.ParallaxPageTransformer;
import com.example.apurv.flick.Animations.ViewPagerScroller;
import com.example.apurv.flick.Fragments.Movie.HomeFragmentMovies;
import com.example.apurv.flick.Listeners.TvLongClick;
import com.example.apurv.flick.Listeners.TvshowCliclListener;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.CategoryConatiner;

import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.ShowAllActivity;
import com.example.apurv.flick.TvDirectory.GenreTv;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvRoot;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.apurv.flick.Animations.ParallaxPageTransformer.ParallaxTransformInformation.PARALLAX_EFFECT_DEFAULT;


public class TvHomeFragment extends Fragment implements  View.OnClickListener, TvshowCliclListener, TvLongClick {
    RecyclerView recyclerViewPopular;
    RecyclerView recyclerViewTopRated;
    RecyclerView recyclerViewUpcoming;

    TvAdapter popularAdapter;
    TvAdapter topratedAdapter;
    TvAdapter upcomingAdapter;
    ProgressBar p;

    MaterialCardView upcomingCard;
    ArrayList<com.example.apurv.flick.TvDirectory.Genre> genres;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 10000; // time in milliseconds between successive task executions.


    ArrayList<TvShow> popularList;
    ArrayList<TvShow> topratedList;
    ArrayList<TvShow> latestlist;
    ArrayList<TvShow> upcoming;

    Button popularshowall;
    Button topratedshowll;
    Button comingsoonshowll;
    Button upcimingshowll;

    ViewPager viewPager;
    ViewPagerTvAdapter latestadapter;

    LinearLayout popularheader;
    View content_root;

    Retrofit.Builder builder;
    Retrofit retrofit;

    HomeFragmentMovies.LoadCompleteListener mListener;

    public TvHomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragmentMovies newInstance()  {
        HomeFragmentMovies fragment = new HomeFragmentMovies();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home_fragment_movies, container, false);


        upcomingCard=view.findViewById(R.id.upcomingCard);
        upcomingCard.setVisibility(View.GONE);

        //Initialize ArrayLists--------------------------------------------------------------------------------------------------------------------
        popularList=new ArrayList<>();
        topratedList=new ArrayList<>();
        latestlist=new ArrayList<>();
        upcoming=new ArrayList<>();
        genres=new ArrayList<>();
//------------------------------------------------------------------------------------------------------------------------------------

//Adapters--------------------------------------------------------------------------------------------------------------------------
        upcomingAdapter=new TvAdapter(getContext(), upcoming,null, "w342", BasePathHolder.posterPathType,this,this,true);
        popularAdapter=new TvAdapter(getContext(), popularList,null, "w342", BasePathHolder.posterPathType,this,this,true);
        topratedAdapter=new TvAdapter(getContext(),topratedList,genres,"w780", BasePathHolder.backdDropPathType,this,this,true);
//--------------------------------------------------------------------------------------------------------------------------------------


//Recycler Views--------------------------------------------------------------------------------------------------------------------
        recyclerViewPopular=view.findViewById(R.id.recycleViewPopular);
        recyclerViewPopular.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recyclerViewPopular.setAdapter(popularAdapter);

        recyclerViewTopRated=view.findViewById(R.id.recycleViewTopRated);
        recyclerViewTopRated.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recyclerViewTopRated.setAdapter(topratedAdapter);

        recyclerViewUpcoming=view.findViewById(R.id.recycleViewUpcoming);
        recyclerViewUpcoming.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recyclerViewUpcoming.setAdapter(upcomingAdapter);
//------------------------------------------------------------------------------------------------------------------------------------

//Progress bar
        p=view.findViewById(R.id.progresBar);
//Toolbar


//Headers-------------------------------------------------------------------------------------------------------------------------------
        popularheader=view.findViewById(R.id.popularheader);
//----------------------------------------------------------------------------------------------------------------------------------------------
        content_root=(View)view. findViewById(R.id.content_root);

        popularshowall=view.findViewById(R.id.popularshowall);
        popularshowall.setOnClickListener(this);

        topratedshowll=view.findViewById(R.id.topratedshowall);
        topratedshowll.setOnClickListener(this);

        upcimingshowll=view.findViewById(R.id.upcomingshowall);
        upcimingshowll.setOnClickListener(this);

        viewPager=view.findViewById(R.id.latestPager);

        latestadapter=new ViewPagerTvAdapter(getChildFragmentManager(),latestlist,genres);
        viewPager.setAdapter(latestadapter);

        fetchMovies(CategoryConatiner.toprated,topratedList,topratedAdapter);
        fetchMovies(CategoryConatiner.popular,popularList,popularAdapter);
        fetchMovies(CategoryConatiner.onAir,latestlist,latestadapter);
        //fetchMovies(CategoryConatiner.upcoming,upcoming,upcomingAdapter);

        ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.BackDrop, 2, 2))
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.cardPoster, -0.58f,
                        PARALLAX_EFFECT_DEFAULT));
        viewPager.setPageTransformer(true, pageTransformer);

    loadmoviegenre();
        changePagerScroller();
        return  view;
    }

    private void fetchMovies(String category, final ArrayList<TvShow> list, final TvAdapter TvAdapter) {
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        TvServices service=retrofit.create(TvServices.class);
        Call<TvRoot> call= service.gettvList(category,1+"");
        call.enqueue(new Callback<TvRoot>() {
            @Override
            public void onResponse(Call<TvRoot> call, Response<TvRoot> response) {
                TvRoot r=response.body();
                ArrayList<TvShow> downloadedTopRatedLists=r.getResults();
                list.addAll(downloadedTopRatedLists);

                TvAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<TvRoot> call, Throwable t) {

            }
        });

    }
    private void loadmoviegenre() {

        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/genre/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        TvServices service=retrofit.create(TvServices.class);
        Call<GenreTv> call= service.getTvGenre();
        call.enqueue(new Callback<GenreTv>() {
            @Override
            public void onResponse(Call<GenreTv> call, Response<GenreTv> response) {
                GenreTv r=response.body();
                ArrayList<com.example.apurv.flick.TvDirectory.Genre> downloadedGenre=r.getGenres();
                genres.addAll(downloadedGenre);
            }

            @Override
            public void onFailure(Call<GenreTv> call, Throwable t) {

            }
        });

    }
    private void fetchMovies(String category, final ArrayList<TvShow> list, final ViewPagerTvAdapter TvAdapter) {
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        TvServices service=retrofit.create(TvServices.class);
        Call<TvRoot> call= service.gettvList(category,1+"");
        call.enqueue(new Callback<TvRoot>() {
            @Override
            public void onResponse(Call<TvRoot> call, Response<TvRoot> response) {
                TvRoot r=response.body();
                ArrayList<TvShow> downloadedTopRatedLists=r.getResults();
                list.addAll(downloadedTopRatedLists);

                p.setVisibility(View.INVISIBLE);
                content_root.setVisibility(View.VISIBLE);
                mListener.onFragmentInteraction();
                TvAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<TvRoot> call, Throwable t) {

            }
        });

        final int NUM_PAGES=latestlist.size();
        final Handler handler;
        handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (viewPager.getCurrentItem() == NUM_PAGES-1) {
                    viewPager.setCurrentItem(0);
                }
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true);
            }
        };

       Timer timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    private void changePagerScroller() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e("tag4", "error of change scroller ", e);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragmentMovies.LoadCompleteListener) {
            mListener = (HomeFragmentMovies.LoadCompleteListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View view) {
        Intent intent=new Intent(getContext(), ShowAllActivity.class);
        intent.putExtra("type",0);
        int id=view.getId();

        switch (id)
        {
            case R.id.popularshowall:intent.putExtra("category", CategoryConatiner.popular);
                break;
            case R.id.topratedshowall:intent.putExtra("category",CategoryConatiner.toprated);
                break;
            case R.id.upcomingshowall: intent.putExtra("category",CategoryConatiner.upcoming);
        }

        startActivity(intent);
    }

    @Override
    public void onClickListener(View v, int position, TvShow obj) {
//        Toast.makeText(getContext(), "Hello", Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(getContext(), TvDetailPage.class);
        intent.putExtra("tvObject",new Gson().toJson(obj).toString());
        startActivity(intent);
    }

    @Override
    public void onLongClickListener(View v, int position, TvShow obj) {

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface LoadCompleteListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
}
