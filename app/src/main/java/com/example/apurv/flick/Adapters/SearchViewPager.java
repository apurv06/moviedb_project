package com.example.apurv.flick.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.FrameLayout;

import com.example.apurv.flick.Fragments.Search.MocieSearchFragment;
import com.example.apurv.flick.Fragments.Search.PeopleFragmentFragment;
import com.example.apurv.flick.Fragments.Search.TvShowFragment;
import com.example.apurv.flick.Services.Search;

public class SearchViewPager extends FragmentPagerAdapter {
    private final String query;

    public SearchViewPager(FragmentManager fm, String query) {
        super(fm);
        this.query=query;
    }

    @Override
    public Fragment getItem(int i) {

        if (i==0)
        {
            return MocieSearchFragment.newInstance(query);
        }
        else if(i==1)
        {
            return TvShowFragment.newInstance(query,query);
        }
        else
        {
            return PeopleFragmentFragment.newInstance(query,query);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int i) {
        if (i==0)
        {
            return "Movie";
        }
        else if(i==1)
        {
            return "Tv";
        }
        else
        {
            return "People";
        }
            }

    @Override
    public int getCount() {
        return 2;
    }
}
