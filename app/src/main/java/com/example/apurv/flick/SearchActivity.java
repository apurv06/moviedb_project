package com.example.apurv.flick;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.example.apurv.flick.Adapters.SearchViewPager;
import com.example.apurv.flick.Fragments.Search.MocieSearchFragment;
import com.example.apurv.flick.Fragments.Search.PeopleFragmentFragment;
import com.example.apurv.flick.Fragments.Search.TvShowFragment;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.Search.Keyword;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.Services.Search;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity implements MocieSearchFragment.OnFragmentInteractionListener,TvShowFragment.OnFragmentInteractionListener,PeopleFragmentFragment.OnFragmentInteractionListener{

        ViewPager viewPager;

    MaterialSearchView searchView;

    ArrayList<String> suggetions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar=findViewById(R.id.toolbar);



        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        //searchView.setBackgroundColor(Color.TRANSPARENT);
        suggetions=new ArrayList<>();

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
//               Toast.makeText(HomeActivity.this, query, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),SearchActivity.class);
                intent.putExtra("query",query);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//               Toast.makeText(HomeActivity.this, newText, Toast.LENGTH_SHORT).show();
                searchView.setVisibility(View.VISIBLE);
                Retrofit.Builder builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/search/").addConverterFactory(GsonConverterFactory.create());
                Retrofit retrofit=builder.build();
                final Search search=retrofit.create(Search.class);
                Call<Keyword> call=search.getKeywords(newText);
                call.enqueue(new Callback<Keyword>() {
                    @Override
                    public void onResponse(Call<Keyword> call, Response<Keyword> response) {
                        suggetions.clear();
                        if(response.body()!=null)
                        { for(int i=0;i<response.body().getResults().size();i++)
                        {
                            suggetions.add(response.body().getResults().get(i).getName());
                        }
                            searchView.setSuggestions(suggetions.toArray(new String[suggetions.size()]));
                        }}

                    @Override
                    public void onFailure(Call<Keyword> call, Throwable t) {

                    }
                });
                return true;
            }
        });
        if(Build.VERSION.SDK_INT >=17){
            searchView.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                searchView.setQuery(suggetions.get(i),false);
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search Results");
        String query=getIntent().getStringExtra("query");
        viewPager=findViewById(R.id.viewPager);
        SearchViewPager pager=new SearchViewPager(getSupportFragmentManager(),query);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(pager);
        TabLayout tabLayout=findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

