package com.example.apurv.flick;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.Database.AppDatabase;
import com.example.apurv.flick.Database.Movie;
import com.example.apurv.flick.Database.MovieDao;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.google.gson.Gson;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavourateMovies extends AppCompatActivity implements CustomItemListner, CustomItemLongClickListener {

        ArrayList<movie> list;
        RecyclerView recyclerView;
        MovieAdapter adapter;
    AppDatabase mDb;
    MovieDao mMovieDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourate_movies);

        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Favourite Movies");

        recyclerView=findViewById(R.id.recycler);
        list=new ArrayList<>();
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL));
        adapter=new MovieAdapter(getApplicationContext(),null, list, "w342", BasePathHolder.posterPathType,this,this,false);

        recyclerView.setAdapter(adapter);

       mDb = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"database_1").allowMainThreadQueries().build();
       mMovieDao = mDb.MovieDao();// Get DAO object
        ArrayList<Movie> obj= (ArrayList<Movie>) mMovieDao.getAll();
        if(obj==null||obj.size()==0)
        {
            Toast.makeText(this, "No Favourite Movies Shows Added", Toast.LENGTH_SHORT).show();

        }
        for (int i=0;i<obj.size();i++)
        {
            loadmovie(obj.get(i).movieid);
//            Toast.makeText(getApplicationContext(), obj.get(i).movieid+"", Toast.LENGTH_SHORT).show();
        }


    }

    public void loadmovie(int id)
    {

        Retrofit.Builder builder=new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.themoviedb.org/3/movie/");

        Retrofit retrofit=builder.build();
        MoviesService services=retrofit.create(MoviesService.class);

        Call<movie> call=services.getMovie(id+"");
        call.enqueue(new Callback<movie>() {
            @Override
            public void onResponse(Call<movie> call, Response<movie> response) {
                movie obj=response.body();
                list.add(obj);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<movie> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClickListener(View v, int position, movie obj) {
        Intent intent=new Intent(getApplicationContext(), MovieDetailPage.class);
        intent.putExtra("movieObject",new Gson().toJson(obj).toString());
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(View v, final int position, final movie obj) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage("Do you want to delete?");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        // Do something after 5s = 5000ms

                            Movie obj1=mMovieDao.Search(obj.getId());

                 mMovieDao.delete(obj1);
                 list.remove(position);
                 adapter.notifyDataSetChanged();

//                        long id = arr.get(pos).getId();
//                        String[] value = {Long.toString(id)};
//                        database.delete(Contract.Table_name, Contract.id + " = ? ", value);
//                        arr.remove(pos);
//                        adapter.notifyDataSetChanged();
//                        setListBack();

//                        Snackbar.make(getCurrentFocus(), "Want To Undo Change?", Snackbar.LENGTH_LONG)
//                                .setAction("UNDO", new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//
//                                        saveExpenseInDatabase(todoobj);
//                                        arr.add(position,todoobj);
//                                        adapter.notifyDataSetChanged();
//                                        return;
//                                    }
//                                })
//                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
//                                .show();



                    }
                }
        );
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
