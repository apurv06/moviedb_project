package com.example.apurv.flick.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.apurv.flick.ImageAndVideo;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.R;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ImageViewPagerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ImageViewPagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageViewPagerFragment extends Fragment implements ImageAndVideo {

    private OnFragmentInteractionListener mListener;
    private String path;
    public static int Image_type=2;

    public void setPoster(boolean poster) {
        isPoster = poster;
    }
    boolean isMovie;
    boolean isPoster;
    public ImageViewPagerFragment() {
        // Required empty public constructor
    }

public void setPath(String path)
{
    this.path=path;
}
    // TODO: Rename and change types and number of parameters
    public static ImageViewPagerFragment newInstance(String param1, String param2) {
        ImageViewPagerFragment fragment = new ImageViewPagerFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View view=inflater.inflate(R.layout.fragment_image_view_pager, container, false);

        ImageView image=view.findViewById(R.id.images);
        Uri uri;
        if (isPoster)
        uri=Uri.parse(BasePathHolder.posterPath+PosterSizes.sizew342+"/"+ path);
        else
            uri=Uri.parse(BasePathHolder.posterPath+PosterSizes.sizew780+"/"+ path);
        Picasso.get().load(uri).into(image);
    return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public int getType() {
        return Image_type;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
