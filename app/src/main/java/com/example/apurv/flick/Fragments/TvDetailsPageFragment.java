package com.example.apurv.flick.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apurv.flick.Adapters.TvDetailsViewPagerAdapter;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Fragments.Tv.TvCastFragment;
import com.example.apurv.flick.Fragments.Tv.TvInfoFragment;
import com.example.apurv.flick.Fragments.Tv.TvReviewsFragment;
import com.example.apurv.flick.Fragments.Tv.TvSeasons;


public class TvDetailsPageFragment extends Fragment implements TvSeasons.OnFragmentInteractionListener,TvReviewsFragment.OnFragmentInteractionListener,TvCastFragment.OnFragmentInteractionListener,TvInfoFragment.OnFragmentInteractionListener {

    private OnFragmentInteractionListener mListener;
    movie obj;
    TabLayout tabLayout;
    ViewPager viewPager;
    TvDetailsViewPagerAdapter adapter;
    View view;
    Bundle bundle;
    public TvDetailsPageFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TvDetailsPageFragment newInstance(String param1, String param2) {
        TvDetailsPageFragment fragment = new TvDetailsPageFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        bundle=getArguments();

        return inflater.inflate(R.layout.fragment_tv_details_page, container, false);
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager=view.findViewById(R.id.detailsViewPager);

        adapter=new TvDetailsViewPagerAdapter(getChildFragmentManager(),bundle);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        adapter.notifyDataSetChanged();
        tabLayout = view.findViewById(R.id.details_tab);
        tabLayout.setupWithViewPager(viewPager);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
