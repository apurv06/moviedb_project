package com.example.apurv.flick.Services;


import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.Search.Keyword;
import com.example.apurv.flick.TvDirectory.TvRoot;

import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Search {
    @GET("keyword?api_key="+ ApiHolder.movieDbApi+"&query=asds&page=1")
        Call<Keyword> getKeywords(@Query("query")String query);

    @GET("movie?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<Movies_rootOfAPI> getMovies(@Query("query")String query,@Query("page")String page);

    @GET("tv?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TvRoot> getTv(@Query("query")String query, @Query("page")String page);
}
