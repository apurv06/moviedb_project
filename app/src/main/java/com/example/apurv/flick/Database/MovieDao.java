package com.example.apurv.flick.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.apurv.flick.MoviesDirectory.movie;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface MovieDao {

    @Query("SELECT * FROM Movie")
    List<Movie> getAll();

    @Query("SELECT * FROM Movie WHERE id IN (:userIds)")
    List<Movie> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM Movie where movieid =(:mid)")
    Movie Search(int mid);

    @Insert(onConflict = REPLACE)
    void insert(Movie... users);

    @Delete
    void delete(Movie user);

    @Insert(onConflict = REPLACE)
    void insert(Movie user);
}
