package com.example.apurv.flick.Adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Listeners.CreditClickListener;
import com.example.apurv.flick.People.Credits.Cast;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PeopleCreditsAdapter extends RecyclerView.Adapter<PeopleCreditsAdapter.MovieCreditsViewHolder> {


    ArrayList<Cast> casts;
    CreditClickListener click;
    public PeopleCreditsAdapter(ArrayList<Cast> casts,CreditClickListener click) {

                   this.click=click;
        this.casts = casts;
    }

    public MovieCreditsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        view=inflater.inflate(R.layout.moviecredits,parent,false);
        final MovieCreditsViewHolder mViewHolder=new MovieCreditsViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click.CreditClickListener(view,mViewHolder.getAdapterPosition());
            }
        });







        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieCreditsViewHolder holder, int position) {

        Uri uri=Uri.parse(BasePathHolder.posterPath+ PosterSizes.sizew342+"/"+casts.get(position).getPosterPath());
        Picasso.get().load(uri).into(holder.image);

        if(casts.get(position).getOriginalTitle()!=null) {
            holder.title.setText(casts.get(position).getOriginalTitle());
            holder.rating.setText(casts.get(position).getReleaseDate());
        }
        else
        {
            holder.title.setText(casts.get(position).getmOriginalName());
            holder.rating.setText(casts.get(position).getFirst_air_date());
        }
        if(casts.get(position).getCharacter()==null||casts.get(position).getCharacter().length()<1)
            holder.role.setText("as Himself");
        else
            holder.role.setText("as "+casts.get(position).getCharacter());

    }

    @Override
    public int getItemCount() {
        return casts.size();
    }

    public class MovieCreditsViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView title;
        TextView role;
        TextView rating;

        public MovieCreditsViewHolder(@NonNull View itemView) {
            super(itemView);

            image=itemView.findViewById(R.id.image);
            title=itemView.findViewById(R.id.title);
            role=itemView.findViewById(R.id.role);
            rating=itemView.findViewById(R.id.rating);
        }
    }
}
