package com.example.apurv.flick.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
@Dao
public interface TvDao {

    @Query("SELECT * FROM Tv")
    List<Tv> getAll();

    @Query("SELECT * FROM Tv WHERE id IN (:userIds)")
    List<Tv> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM Tv where tvid =(:mid)")
    Tv Search(Long mid);

    @Insert(onConflict = REPLACE)
    void insert(Tv... users);

    @Delete
    void delete(Tv user);

    @Insert(onConflict = REPLACE)
    void insert(Tv user);
}
