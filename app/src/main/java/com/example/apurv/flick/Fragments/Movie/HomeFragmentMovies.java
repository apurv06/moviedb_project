package com.example.apurv.flick.Fragments.Movie;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.ViewPagerMoviesAdapter;
import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.Animations.ViewPagerScroller;
import com.example.apurv.flick.Database.AppDatabase;
import com.example.apurv.flick.Database.Movie;
import com.example.apurv.flick.Database.MovieDao;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.CategoryConatiner;
import com.example.apurv.flick.MoviesDirectory.Genre.Genre;
import com.example.apurv.flick.MoviesDirectory.Genre.GenreMovie;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.Animations.ParallaxPageTransformer;
import com.example.apurv.flick.R;
import com.example.apurv.flick.ShowAllActivity;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.apurv.flick.Animations.ParallaxPageTransformer.ParallaxTransformInformation.PARALLAX_EFFECT_DEFAULT;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragmentMovies.LoadCompleteListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragmentMovies#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragmentMovies extends Fragment implements View.OnClickListener,CustomItemListner,CustomItemLongClickListener {

    int currentPage = 0;
    Timer timer;
    ArrayList<Genre> moviesGenre;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 10000; // time in milliseconds between successive task executions.

        RecyclerView recyclerViewPopular;
        RecyclerView recyclerViewTopRated;
         RecyclerView recyclerViewUpcoming;

        MovieAdapter popularAdapter;
        MovieAdapter topratedAdapter;
        MovieAdapter upcomingAdapter;
        ProgressBar p;


        ArrayList<movie> popularList;
        ArrayList<movie> topratedList;
        ArrayList<movie> latestlist;
        ArrayList<movie> upcoming;

        Button popularshowall;
        Button topratedshowll;
        Button upcimingshowll;


        ViewPager viewPager;
        ViewPagerMoviesAdapter latestadapter;

        LinearLayout popularheader;
        View content_root;

        Retrofit.Builder builder;
        Retrofit retrofit;

        LoadCompleteListener mListener;

    public HomeFragmentMovies() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragmentMovies newInstance()  {
        HomeFragmentMovies fragment = new HomeFragmentMovies();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)

    {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home_fragment_movies, container, false);


        //Initialize ArrayLists--------------------------------------------------------------------------------------------------------------------
        popularList=new ArrayList<>();
        topratedList=new ArrayList<>();
        latestlist=new ArrayList<>();
        upcoming=new ArrayList<>();
        moviesGenre=new ArrayList<>();
//------------------------------------------------------------------------------------------------------------------------------------

        loadmoviegenre();
//Adapters--------------------------------------------------------------------------------------------------------------------------
        popularAdapter=new MovieAdapter(getContext(),null, popularList, "w342", BasePathHolder.posterPathType,this,this,true);
        upcomingAdapter=new MovieAdapter(getContext(),null, upcoming, "w342", BasePathHolder.posterPathType,this,this,true);
        topratedAdapter=new MovieAdapter(getContext(),moviesGenre,topratedList,"w780", BasePathHolder.backdDropPathType,this,this,true);
//--------------------------------------------------------------------------------------------------------------------------------------


//Recycler Views--------------------------------------------------------------------------------------------------------------------
        recyclerViewPopular=view.findViewById(R.id.recycleViewPopular);
        recyclerViewPopular.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recyclerViewPopular.setAdapter(popularAdapter);

        recyclerViewTopRated=view.findViewById(R.id.recycleViewTopRated);
        recyclerViewTopRated.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recyclerViewTopRated.setAdapter(topratedAdapter);

        recyclerViewUpcoming=view.findViewById(R.id.recycleViewUpcoming);
        recyclerViewUpcoming.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recyclerViewUpcoming.setAdapter(upcomingAdapter);
//------------------------------------------------------------------------------------------------------------------------------------

//Progress bar
        p=view.findViewById(R.id.progresBar);
//Toolbar


//Headers-------------------------------------------------------------------------------------------------------------------------------
        popularheader=view.findViewById(R.id.popularheader);
//----------------------------------------------------------------------------------------------------------------------------------------------
        content_root=(View)view. findViewById(R.id.content_root);

        popularshowall=view.findViewById(R.id.popularshowall);
            popularshowall.setOnClickListener(this);

        topratedshowll=view.findViewById(R.id.topratedshowall);
        topratedshowll.setOnClickListener(this);

        upcimingshowll=view.findViewById(R.id.upcomingshowall);
        upcimingshowll.setOnClickListener(this);

        viewPager=view.findViewById(R.id.latestPager);

        latestadapter=new ViewPagerMoviesAdapter(getFragmentManager(),latestlist,moviesGenre);
        viewPager.setAdapter(latestadapter);

        fetchMovies(CategoryConatiner.toprated,topratedList,topratedAdapter);
        fetchMovies(CategoryConatiner.popular,popularList,popularAdapter);
        fetchMovies(CategoryConatiner.now_playing,latestlist,latestadapter);
        fetchMovies(CategoryConatiner.upcoming,upcoming,upcomingAdapter);


        ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.BackDrop, 2, 2))
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.cardPoster, -0.58f,
                        PARALLAX_EFFECT_DEFAULT));
        viewPager.setPageTransformer(true, pageTransformer);


changePagerScroller();
        return  view;
    }

    private void fetchMovies(String category, final ArrayList<movie> list, final MovieAdapter movieAdapter) {
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<Movies_rootOfAPI> call= service.getMovieList(category,1+"");
        call.enqueue(new Callback<Movies_rootOfAPI>() {
            @Override
            public void onResponse(Call<Movies_rootOfAPI> call, Response<Movies_rootOfAPI> response) {
                Movies_rootOfAPI r=response.body();
                ArrayList<movie> downloadedTopRatedLists=r.getResults();
                list.addAll(downloadedTopRatedLists);


                    movieAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<Movies_rootOfAPI> call, Throwable t) {

            }
        });

    }


    private void fetchMovies(String category, final ArrayList<movie> list, final ViewPagerMoviesAdapter movieAdapter) {
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<Movies_rootOfAPI> call= service.getMovieList(category,1+"");
        call.enqueue(new Callback<Movies_rootOfAPI>() {
            @Override
            public void onResponse(Call<Movies_rootOfAPI> call, Response<Movies_rootOfAPI> response) {
                Movies_rootOfAPI r=response.body();
                ArrayList<movie> downloadedTopRatedLists=r.getResults();
                list.addAll(downloadedTopRatedLists);

                p.setVisibility(View.INVISIBLE);
                content_root.setVisibility(View.VISIBLE);
                mListener.onFragmentInteraction();
                movieAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<Movies_rootOfAPI> call, Throwable t) {

            }
        });


        final int NUM_PAGES=latestlist.size();
        final Handler handler;
        handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (viewPager.getCurrentItem() == NUM_PAGES-1) {
                    viewPager.setCurrentItem(0);
                }
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }
    private void loadmoviegenre() {

        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/genre/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<GenreMovie> call= service.getMovieGenre();
        call.enqueue(new Callback<GenreMovie>() {
            @Override
            public void onResponse(Call<GenreMovie> call, Response<GenreMovie> response) {
                GenreMovie r=response.body();
                ArrayList<Genre> downloadedGenre=r.getGenres();
                moviesGenre.addAll(downloadedGenre);
            }

            @Override
            public void onFailure(Call<GenreMovie> call, Throwable t) {

            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoadCompleteListener) {
            mListener = (LoadCompleteListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickListener(View v, int position,movie obj) {

        Intent intent=new Intent(getContext(), MovieDetailPage.class);
        intent.putExtra("movieObject",new Gson().toJson(obj).toString());
        startActivity(intent);

    }

    @Override
    public void onItemLongClick(View v, int position,movie obj) {


//        AppDatabase mDb = Room.databaseBuilder(getContext(), AppDatabase.class,"database_1").allowMainThreadQueries().build();
//        MovieDao mMovieDao = mDb.MovieDao();// Get DAO object
//        Movie obj1=mMovieDao.Search(obj.getId());
//        if(obj1!=null) {
//            Movie movie = new Movie(obj.getId());
//            Toast.makeText(getContext(), "Added To Favourites", Toast.LENGTH_SHORT).show();
//            mMovieDao.insert(movie); // Insert it in database
//        }
//        else
//        {
//            Toast.makeText(getContext(), "Already In Favourites", Toast.LENGTH_SHORT).show();
//        }
    }
    private void changePagerScroller() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e("tag4", "error of change scroller ", e);
        }
    }
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(getContext(), ShowAllActivity.class);
        intent.putExtra("type",1);
        int id=view.getId();

        switch (id)
        {
            case R.id.popularshowall:intent.putExtra("category", CategoryConatiner.popular);
            break;
            case R.id.topratedshowall:intent.putExtra("category",CategoryConatiner.toprated);
            break;
            case R.id.upcomingshowall:intent.putExtra("category",CategoryConatiner.upcoming);
                break;
        }

        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface LoadCompleteListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }

}
