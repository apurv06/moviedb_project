package com.example.apurv.flick.Adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.apurv.flick.Fragments.OnGoingFragment;
import com.example.apurv.flick.MoviesDirectory.Genre.Genre;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;

import java.util.ArrayList;

public class ViewPagerTvAdapter extends FragmentPagerAdapter {
    ArrayList<TvShow> list;
    ArrayList<com.example.apurv.flick.TvDirectory.Genre> moviesGenre;
    public ViewPagerTvAdapter(FragmentManager fm, ArrayList<TvShow> list,ArrayList<com.example.apurv.flick.TvDirectory.Genre> moviesGenre) {
        super(fm);
        this.moviesGenre=moviesGenre;
        this.list=list;

    }

    @Override
    public Fragment getItem(int position) {

        OnGoingFragment fragment=new OnGoingFragment();
        String temp="";
        list.get(position).setGenres(moviesGenre);
        ArrayList<String> Genre = list.get(position).getGenres();
        for (int i = 0; i < Genre.size(); i++) {
            String genre = Genre.get(i);
            temp=temp+(genre + " ");
        }
        fragment.setPoster(list.get(position).getPosterPath());
        fragment.setBackdrop(list.get(position).getBackdropPath());
        fragment.setTitle(list.get(position).getName());
        fragment.setGenre(temp);
        Bundle bundle=new Bundle();
        bundle.putInt("type",list.get(position).getType());
        bundle.putSerializable("Object",new Gson().toJson(list.get(position)).toString());
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public int getCount() {
        return list.size();
    }
}
