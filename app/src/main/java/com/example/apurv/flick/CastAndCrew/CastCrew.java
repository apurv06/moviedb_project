
package com.example.apurv.flick.CastAndCrew;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;


public class CastCrew {

    @Expose
    private ArrayList<Cast> cast;
    @Expose
    private ArrayList<Crew> crew;
    @Expose
    private Long id;

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

    public List<Crew> getCrew() {
        return crew;
    }

    public void setCrew(ArrayList<Crew> crew) {
        this.crew = crew;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
