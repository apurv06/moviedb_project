
package com.example.apurv.flick.Images;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ImagesRoot {

    @SerializedName("backdrops")
    private ArrayList<Backdrop> mBackdrops;
    @SerializedName("id")
    private Long mId;
    @SerializedName("posters")
    private ArrayList<Poster> mPosters;

    public ArrayList<Backdrop> getBackdrops() {
        return mBackdrops;
    }

    public void setBackdrops(ArrayList<Backdrop> backdrops) {
        mBackdrops = backdrops;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public ArrayList<Poster> getPosters() {
        return mPosters;
    }

    public void setPosters(ArrayList<Poster> posters) {
        mPosters = posters;
    }

}
