package com.example.apurv.flick.Fragments.Tv;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.EpisodeRecycler;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.TvDirectory.Episode;
import com.example.apurv.flick.TvDirectory.Season;
import com.example.apurv.flick.TvDirectory.TvDetailsForSeason;
import com.example.apurv.flick.TvDirectory.TvSeason;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TvSeasons.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TvSeasons#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TvSeasons extends Fragment implements AdapterView.OnItemSelectedListener {

    Bundle bundle;
    View view;
    ImageView imageView;
    TvShow obj;
    ArrayAdapter<String> dataAdapter;
    ArrayList<String> contain;
    ArrayList<Season> seasons;
    Spinner spinner;

    ExpandableTextView overview;
    TextView release;

    RecyclerView recyclerView;
    EpisodeRecycler adapter;
    ArrayList<Episode> list;
    private OnFragmentInteractionListener mListener;

    public TvSeasons() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TvSeasons newInstance(Bundle bundle) {
        TvSeasons fragment = new TvSeasons();
        Bundle args = bundle;


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        bundle=getArguments();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_tv_seasons, container, false);
        bundle = getArguments();
        recyclerView=view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list=new ArrayList<>();
        adapter=new EpisodeRecycler(list);
        recyclerView.setAdapter(adapter);
        obj=new Gson().fromJson(bundle.getString("tvObject"),TvShow.class);
        seasons=new ArrayList<>();
    imageView=view.findViewById(R.id.image);


        overview= (ExpandableTextView) view.findViewById(R.id.sample2)
                .findViewById(R.id.expand_text_view);

        overview.setBackgroundColor(Color.BLACK);
        release=view.findViewById(R.id.release_date);
               contain=new ArrayList<>();
        fetchDetails();

        spinner= (Spinner) view.findViewById(R.id.spinner);

       dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, contain);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setOnItemSelectedListener(this);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setPrompt("Seasons");
        return view;
    }
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        if (seasons.get(position).getOverview()==null||seasons.get(position).getOverview().toString().length()==0)
            overview.setVisibility(View.GONE);
        else
            overview.setText("Overview\n"+seasons.get(position).getOverview());

        release.setText("AirDate: "+seasons.get(position).getAirDate());
        Uri uri=Uri.parse(BasePathHolder.posterPath+ PosterSizes.sizew342+"/"+seasons.get(position).getPosterPath());
        Picasso.get().load(uri).into(imageView);
        if(seasons.size()>0)
        loadEpisodes(seasons.get(position).getSeasonNumber());
        else
            loadEpisodes(0L);
        // Showing selected spinner item
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    private void loadEpisodes(Long seasonNumber) {

        Retrofit.Builder builder=new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.themoviedb.org/3/tv/");
        Retrofit retrofit=builder.build();
        TvServices services=retrofit.create(TvServices.class);
        Call<TvSeason> call=services.getTvEpisodes(obj.getId()+"",seasonNumber+"");
        call.enqueue(new Callback<TvSeason>() {
            @Override
            public void onResponse(Call<TvSeason> call, Response<TvSeason> response) {
                list.clear();
                list.addAll(response.body().getEpisodes());
                adapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<TvSeason> call, Throwable t) {
//                Toast.makeText(getContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }



    private void fetchDetails() {


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        Retrofit.Builder builder=new Retrofit.Builder().client(client).addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.themoviedb.org/3/tv/");
        Retrofit retrofit=builder.build();
        TvServices services=retrofit.create(TvServices.class);
        Call<TvDetailsForSeason> call=services.getTvShowDetails(obj.getId()+"");
        call.enqueue(new Callback<TvDetailsForSeason>() {
            @Override
            public void onResponse(Call<TvDetailsForSeason> call, Response<TvDetailsForSeason> response) {
                seasons.addAll(response.body().getSeasons());

                for(int i=0;i<seasons.size();i++)
                {
                    contain.add("Season "+seasons.get(i).getSeasonNumber());
                }

                    dataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TvDetailsForSeason> call, Throwable t) {
//                Toast.makeText(getContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

