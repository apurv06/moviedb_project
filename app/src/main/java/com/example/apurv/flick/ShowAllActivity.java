package com.example.apurv.flick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.MediaType;
import com.example.apurv.flick.Adapters.TvAdapter;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.Listeners.EndlessRecyclerViewScrollListener;
import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.Listeners.TvshowCliclListener;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvRoot;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShowAllActivity extends AppCompatActivity  {
    private EndlessRecyclerViewScrollListener scrollListener;
    ArrayList<movie> list;
    ArrayList<TvShow> listTv;
    Retrofit.Builder builder;
    Retrofit retrofit;
    int type;
    String category;

    MovieAdapter adapter;
    TvAdapter adapterTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all);


        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        type=getIntent().getIntExtra("type",-1);


        category=getIntent().getStringExtra("category");
        if(category.equals(CategoryConatiner.toprated))
            getSupportActionBar().setTitle("Top Rated");
        else if(category.equals(CategoryConatiner.popular))
        {
            getSupportActionBar().setTitle("Popular");
        }
        else
        {
            getSupportActionBar().setTitle("Upcoming");
        }
        RecyclerView rvItems = (RecyclerView) findViewById(R.id.list);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        rvItems.setLayoutManager(layoutManager);
        if(type==0)
        {

            listTv=new ArrayList<>();
            adapterTv=new TvAdapter(this, listTv, null, "w342", BasePathHolder.posterPathType, new TvshowCliclListener() {
                @Override
                public void onClickListener(View v, int position, TvShow obj) {
                    Intent intent = new Intent(ShowAllActivity.this,TvDetailPage.class);
                    intent.putExtra("tvObject", new Gson().toJson(obj).toString());
                    startActivity(intent);
                }
            }, null, false);
            rvItems.setAdapter(new AlphaInAnimationAdapter(adapterTv));
        }
        else
        {
            list=new ArrayList<>();
            adapter=new MovieAdapter(this, null, list, "w342", BasePathHolder.posterPathType, new CustomItemListner() {
                @Override
                public void onClickListener(View v, int position, movie obj) {
                    Intent intent = new Intent(ShowAllActivity.this, MovieDetailPage.class);
                    intent.putExtra("movieObject", new Gson().toJson(obj).toString());
                    startActivity(intent);
                }
            }, new CustomItemLongClickListener() {
                @Override
                public void onItemLongClick(View v, int position, movie obj) {

                }
            }, false);
            rvItems.setAdapter(adapter);
        }
            
        
        loadNextDataFromApi(1);
       
        
           
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                loadNextDataFromApi(page+1);
            }
        };
        // Adds the scroll listener to RecyclerView
        rvItems.addOnScrollListener(scrollListener);

    }

    private void loadNextDataFromApi(int i) {
        if(type==0)
        {
            loadNextDataFromApiTv(i);
        }
        else{
            loadNextDataFromApiMovie(i);
        }
    }

    private void loadNextDataFromApiTv(int offset) {
        builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.build();
        TvServices service = retrofit.create(TvServices.class);
        Call<TvRoot> call = service.gettvList(category,offset+"");
//        Toast.makeText(getApplicationContext(), "In", Toast.LENGTH_SHORT).show();
        call.enqueue(new Callback<TvRoot>() {
            @Override
            public void onResponse(Call<TvRoot> call, Response<TvRoot> response) {
                TvRoot r = response.body();
                ArrayList<TvShow> downloadedPopularLists = r.getResults();
                listTv.addAll(downloadedPopularLists);


                adapterTv.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TvRoot> call, Throwable t) {

            }
        });
    }

    public void loadNextDataFromApiMovie(int offset) {
        builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.build();
        MoviesService service = retrofit.create(MoviesService.class);
        Call<Movies_rootOfAPI> call = service.getMovieList(category,offset+"");
//        Toast.makeText(getApplicationContext(), "In", Toast.LENGTH_SHORT).show();
        call.enqueue(new Callback<Movies_rootOfAPI>() {
            @Override
            public void onResponse(Call<Movies_rootOfAPI> call, Response<Movies_rootOfAPI> response) {
                Movies_rootOfAPI r = response.body();
                ArrayList<movie> downloadedPopularLists = r.getResults();
                list.addAll(downloadedPopularLists);


                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Movies_rootOfAPI> call, Throwable t) {

            }
        });
    }


}
