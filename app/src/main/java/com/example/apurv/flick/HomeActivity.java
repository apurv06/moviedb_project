package com.example.apurv.flick;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.HomeActivityViewAdapter;
import com.example.apurv.flick.Fragments.Movie.HomeFragmentMovies;
import com.example.apurv.flick.Fragments.Tv.TvHomeFragment;

import com.example.apurv.flick.Search.Keyword;
import com.example.apurv.flick.Services.Search;
import com.miguelcatalan.materialsearchview.MaterialSearchView;


import java.util.ArrayList;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener,HomeFragmentMovies.LoadCompleteListener,TvHomeFragment.LoadCompleteListener {

    android.support.v7.widget.Toolbar toolbar;
    android.support.design.widget.AppBarLayout appBarLayout;
    ViewPager viewPager;
    HomeActivityViewAdapter adapter;
    TabLayout tabLayout;

    MaterialSearchView searchView;

    ArrayList<String> suggetions;

    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       searchView = (MaterialSearchView) findViewById(R.id.search_view);
        //searchView.setBackgroundColor(Color.TRANSPARENT);
       suggetions=new ArrayList<>();

        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        fab.setOnClickListener(this);


       searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
           @Override
           public boolean onQueryTextSubmit(String query) {
//               Toast.makeText(HomeActivity.this, query, Toast.LENGTH_SHORT).show();
               Intent intent=new Intent(getApplicationContext(),SearchActivity.class);
               intent.putExtra("query",query);
               startActivity(intent);
               return true;
           }

           @Override
           public boolean onQueryTextChange(String newText) {
//               Toast.makeText(HomeActivity.this, newText, Toast.LENGTH_SHORT).show();
                searchView.setVisibility(View.VISIBLE);
               Retrofit.Builder builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/search/").addConverterFactory(GsonConverterFactory.create());
                Retrofit retrofit=builder.build();
               final Search search=retrofit.create(Search.class);
               Call<Keyword> call=search.getKeywords(newText);
               call.enqueue(new Callback<Keyword>() {
                   @Override
                   public void onResponse(Call<Keyword> call, Response<Keyword> response) {
                       suggetions.clear();
                       if(response.body()!=null)
                       { for(int i=0;i<response.body().getResults().size();i++)
                       {
                           suggetions.add(response.body().getResults().get(i).getName());
                       }
                       searchView.setSuggestions(suggetions.toArray(new String[suggetions.size()]));
                   }}

                   @Override
                   public void onFailure(Call<Keyword> call, Throwable t) {

                   }
               });
               return true;
           }
       });
        if(Build.VERSION.SDK_INT >=17){
            searchView.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

       searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               searchView.setQuery(suggetions.get(i),false);
           }
       });
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appBarLayout=findViewById(R.id.appBar);


        viewPager=findViewById(R.id.viewPager);
        adapter=new HomeActivityViewAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        tabLayout = findViewById(R.id.home_tab);
        tabLayout.setupWithViewPager(viewPager);

//        FragmentManager fragmentManager=getSupportFragmentManager();
//        HomeFragmentMovies homeFragmentMovies=HomeFragmentMovies.newInstance();
//
//        FragmentTransaction transaction= fragmentManager.beginTransaction();
//        transaction.add(R.id.homeFrame,homeFragmentMovies);
//        transaction.commit();



    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.fab:

                animateFAB();
                break;
            case R.id.fab1:

                Log.d("Raj", "Fab 1");
                break;
            case R.id.fab2:

                Log.d("Raj", "Fab 2");
                break;
        }


    }
    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            // Close the search on the back button press.
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public void onFragmentInteraction() {
        appBarLayout.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
    }

    public void getfavourates(View view) {
        Intent intent=new Intent(getApplicationContext(),FavourateMovies.class);
        startActivity(intent);
    }

    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }

    public void getTvFav(View view) {
        Intent intent=new Intent(getApplicationContext(),FavourateTvShow.class);
        startActivity(intent);
    }
}
