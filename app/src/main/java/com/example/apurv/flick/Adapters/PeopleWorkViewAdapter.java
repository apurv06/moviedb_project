package com.example.apurv.flick.Adapters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.apurv.flick.Fragments.People.PeopleInfoFragment;
import com.example.apurv.flick.Fragments.People.PeopleMoviesFragment;
import com.example.apurv.flick.Fragments.People.PeopleTvFragment;

public class PeopleWorkViewAdapter extends FragmentPagerAdapter {
    Bundle bundle;

    public PeopleWorkViewAdapter(FragmentManager fm,Bundle bundle) {
        super(fm);
        this.bundle=bundle;
    }

    @Override
    public Fragment getItem(int i) {
        if(i==0)
        {
            return PeopleInfoFragment.newInstance(bundle);
        }
        else if(i==1)
        {
            return PeopleMoviesFragment.newInstance(bundle);
        }
        else
        {
            return PeopleTvFragment.newInstance(bundle);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0)
        {
            return "Info";
        }
        else if (position==1)
            return  "Movies";
        else
            return  "Tv";
    }

    @Override
    public int getCount() {
        return 3;
    }
}
