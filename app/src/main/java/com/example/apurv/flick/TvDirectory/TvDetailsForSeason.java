
package com.example.apurv.flick.TvDirectory;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvDetailsForSeason {


    @SerializedName("seasons")
    private List<Season> mSeasons;


    public List<Season> getSeasons() {
        return mSeasons;
    }

    public void setSeasons(List<Season> seasons) {
        mSeasons = seasons;
    }


}
