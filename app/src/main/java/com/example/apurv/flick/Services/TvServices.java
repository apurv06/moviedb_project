package com.example.apurv.flick.Services;

import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.CastAndCrew.CastCrew;
import com.example.apurv.flick.MoviesDirectory.Video_Path_API;
import com.example.apurv.flick.TvDirectory.GenreTv;
import com.example.apurv.flick.TvDirectory.TvDetailsForSeason;
import com.example.apurv.flick.TvDirectory.TvRoot;
import com.example.apurv.flick.TvDirectory.TvSeason;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.example.apurv.flick.imdb.OmdbTv;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TvServices {

    @GET("{category}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TvRoot> gettvList(@Path("category") String cat, @Query("page") String page);

    @GET("{tv_id}/videos?api_key="+ ApiHolder.movieDbApi+"&language=en-US")
    Call<Video_Path_API> getTrailerPath(@Path("tv_id") String tvId);

    @GET("{tv_id}/credits?api_key="+ApiHolder.movieDbApi+"")
    Call<CastCrew> getCastAndCrew(@Path("tv_id") String tvId);

    @GET("{tv_id}/{type}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TvRoot> getSuggestedtvs(@Path("tv_id") String tvId, @Path("type") String type);

    @GET("list?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<GenreTv> getTvGenre();

    @GET("?&apikey="+ApiHolder.omdbDbApi+"")
    Call<OmdbTv> getInofromOmdb(@Query("i") String imdb_id);


    @GET("{tv_id}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TvShow> getTvShow(@Path("tv_id") String tvId);

    @GET("{tv_id}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TvDetailsForSeason> getTvShowDetails(@Path("tv_id") String tvId);

    @GET("{tvid}/season/{season_number}?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TvSeason> getTvEpisodes(@Path("tvid") String s,@Path("season_number") String s1);
}


