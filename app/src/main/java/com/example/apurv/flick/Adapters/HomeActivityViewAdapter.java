package com.example.apurv.flick.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.apurv.flick.Fragments.Movie.HomeFragmentMovies;
import com.example.apurv.flick.Fragments.Tv.TvHomeFragment;

public class HomeActivityViewAdapter extends FragmentPagerAdapter {
    public HomeActivityViewAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        if(i==0)
        {
            return new HomeFragmentMovies();
        }
        else
        {
            return new TvHomeFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        if (position==0)
            return "Movies";
        else
            return "Tv Shows";
    }
}
