package com.example.apurv.flick.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.Fragments.ImageViewPagerFragment;
import com.example.apurv.flick.ImageAndVideo;
import com.example.apurv.flick.Images.Backdrop;
import com.example.apurv.flick.Images.Poster;
import com.example.apurv.flick.MoviesDirectory.video;
import com.example.apurv.flick.Fragments.PlayerYouTubeFrag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ImagesVideosAdapter extends FragmentStatePagerAdapter
        implements PlayerYouTubeFrag.MediaFullScreenListener {

    protected ArrayList<ImageAndVideo> mResources;
    private String apiKey = ""+ ApiHolder.youtubeDbApi+"";
    protected Map<Integer, PlayerYouTubeFrag.InitializeListener> listenerMap = new HashMap<>();
    private Context context;

    private boolean isFullScreen;

    public ImagesVideosAdapter(FragmentManager fm, ArrayList<ImageAndVideo> mResources, Context context) {
        super(fm);
        this.context = context;
        this.mResources=mResources;
    }

    public int currentPosition = -1;


    @Override
    public void startFullScreen() {
        if (!isFullScreen){

        }
    }

    @Override
    public Fragment getItem(int position) {

        if(getType(position)==3){
            ImageViewPagerFragment fragment=new ImageViewPagerFragment();
            Poster obj= (Poster) mResources.get(position);
            fragment.setPath(obj.getFilePath());
            fragment.setPoster(true);
            return fragment;
        }
        else if(getType(position)==2)
        {

            ImageViewPagerFragment fragment=new ImageViewPagerFragment();
            Backdrop obj= (Backdrop) mResources.get(position);
            fragment.setPath(obj.getFilePath());
            fragment.setPoster(false);
            return fragment;

        }
        else
        {
            Log.i("tag3", "getItem video" + String.valueOf(position));
           video obj= (video) mResources.get(position);
            PlayerYouTubeFrag youTubePlayerSupportFragment =
                    PlayerYouTubeFrag.newInstance(obj.getKey(), this);
            listenerMap.put(position, youTubePlayerSupportFragment.getmInitializeListener());
            if(position==0)
                onPageChanged(0);
            return youTubePlayerSupportFragment;
        }
    }

    private int getType(int position) {

        return mResources.get(position).getType();
    }

    public void onPageChanged(int position){
        // current page it not null, realse youtubeplayer.
        if(currentPosition != -1){
            if(listenerMap.get(currentPosition) != null){
                listenerMap.get(currentPosition).stopVideo();
            }
        }
        // update current position
        currentPosition = position;
        Log.i("tag3", "onPageSelected " + String.valueOf(position));

        //start loading video
        PlayerYouTubeFrag.InitializeListener listener = listenerMap.get(position);
        if(listener != null){
            Log.i("tag3", "startVideo " + String.valueOf(position));
            listener.startVideo();
        } else {
            Log.i("tag3", "startVideo listener is null " + String.valueOf(position));
        }
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    public ArrayList<ImageAndVideo> getmResources() {
        return mResources;
    }

    public void setmResources(ArrayList<ImageAndVideo> mResources) {
        this.mResources = mResources;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
}
