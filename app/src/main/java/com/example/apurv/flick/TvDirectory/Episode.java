
package com.example.apurv.flick.TvDirectory;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Episode {


    @SerializedName("episode_number")
    private Long mEpisodeNumber;

    @SerializedName("name")
    private String mName;

String still_path;

    public String getStill_path() {
        return still_path;
    }

    public void setStill_path(String still_path) {
        this.still_path = still_path;
    }

    public String getVote_average() {
        return vote_average;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }

    String   vote_average;


    public Long getEpisodeNumber() {
        return mEpisodeNumber;
    }

    public void setEpisodeNumber(Long episodeNumber) {
        mEpisodeNumber = episodeNumber;
    }



    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }



}
