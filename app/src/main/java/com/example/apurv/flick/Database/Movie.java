package com.example.apurv.flick.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Movie {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public Movie(int movieid) {
        this.movieid = movieid;
    }

   public int movieid;


}
