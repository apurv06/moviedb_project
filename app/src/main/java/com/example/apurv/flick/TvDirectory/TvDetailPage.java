package com.example.apurv.flick.TvDirectory;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.ImagesVideosAdapter;
import com.example.apurv.flick.Database.AppDatabase;
import com.example.apurv.flick.Database.Movie;
import com.example.apurv.flick.Database.MovieDao;
import com.example.apurv.flick.Database.Tv;
import com.example.apurv.flick.Database.TvDao;
import com.example.apurv.flick.Fragments.ImageViewPagerFragment;
//import com.example.apurv.flick.Fragments.TvDetailsPageFragment;
import com.example.apurv.flick.Fragments.Tv.TvCastFragment;
import com.example.apurv.flick.Fragments.Tv.TvInfoFragment;
import com.example.apurv.flick.Fragments.Tv.TvReviewsFragment;
import com.example.apurv.flick.Fragments.Tv.TvSeasons;
import com.example.apurv.flick.Fragments.TvDetailsPageFragment;
import com.example.apurv.flick.HomeActivity;
import com.example.apurv.flick.ImageAndVideo;
import com.example.apurv.flick.Images.ImagesRoot;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.Video_Path_API;
import com.example.apurv.flick.MoviesDirectory.video;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.Settings;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TvDetailPage extends AppCompatActivity implements View.OnClickListener,ImageViewPagerFragment.OnFragmentInteractionListener,TvCastFragment.OnFragmentInteractionListener,TvDetailsPageFragment.OnFragmentInteractionListener,TvReviewsFragment.OnFragmentInteractionListener,TvSeasons.OnFragmentInteractionListener,TvInfoFragment.OnFragmentInteractionListener {

    ArrayList<ImageAndVideo> tvvideos;
    ArrayList<ImageAndVideo> tvimages;
    ArrayList<ImageAndVideo> arr;
    android.support.v7.widget.Toolbar toolbar;
    ImagesVideosAdapter imagesVideosAdapter;
    AppBarLayout appBarLayout;
    YouTubePlayerFragment myYouTubePlayerFragment;
    FrameLayout frameLayout;
    String video_key;
    Bundle bundle;
    TvShow obj;
    RadioGroup radioGroup;
    ViewPager viewPager;

    CheckBox checkBox;
    Boolean flag=false;
    int ImagesPosition;

    RadioButton videosButton;
    RadioButton imagesButton;
    AppDatabase mDb;
    TvDao mMovieDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_detail_page);
        Intent intent = getIntent();
        bundle = intent.getExtras();
        obj = new Gson().fromJson(bundle.getString("tvObject"), TvShow.class);
        tvvideos = new ArrayList<>();
        tvimages = new ArrayList<>();
        arr = new ArrayList<>();
        loadTrailer();
        checkBox=findViewById(R.id.favourite);
        mDb = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"database_1").allowMainThreadQueries().build();
        mMovieDao = mDb.TvDao();// Get DAO object
        Tv obj1=mMovieDao.Search(obj.getId());
        if (obj1!=null)
        {
            checkBox.setChecked(true);
        }


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {

                    Tv movie = new Tv(obj.getId());

                    mMovieDao.insert(movie); // Insert it in database
                    Toast.makeText(TvDetailPage.this, "Added To Favourites", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(TvDetailPage.this, "Removed From Favourites", Toast.LENGTH_SHORT).show();
                   Tv obj1=mMovieDao.Search(obj.getId());
                    mMovieDao.delete(obj1);
                }
            }
        });

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TvDetailsPageFragment fragment = new TvDetailsPageFragment();
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.add(R.id.contentFrame, fragment);
        transaction.commit();

        viewPager = findViewById(R.id.viewPager);
        imagesVideosAdapter = new ImagesVideosAdapter(getSupportFragmentManager(), arr, TvDetailPage.this);
        viewPager.setAdapter(imagesVideosAdapter);

        videosButton = findViewById(R.id.Videos);
        imagesButton = findViewById(R.id.images);

        if (Settings.default_Header_details_page == video.Type) {
            videosButton.setChecked(true);
        } else {
            imagesButton.setChecked(true);
        }

        final ViewPager.OnPageChangeListener pagerchangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                imagesVideosAdapter.onPageChanged(position);

                if (arr.get(position).getType() == video.Type) {
                    videosButton.setChecked(true);
                    flag=true;

                } else {
                    imagesButton.setChecked(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        viewPager.addOnPageChangeListener(pagerchangeListener);


        radioGroup = findViewById(R.id.radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(flag) {
                    flag=false;
                    if (i == R.id.Videos) {
                        viewPager.setCurrentItem(0, true);

                    } else {
                        viewPager.setCurrentItem(ImagesPosition, true);
                    }
                }
                else
                {
                    if (i == R.id.Videos) {
                        viewPager.setCurrentItem(tvvideos.size()-1, true);

                    } else {
                        viewPager.setCurrentItem(ImagesPosition, true);
                    }
                }
            }
        });


    }


    private void loadTrailer() {

        Retrofit.Builder builder;
        builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        MoviesService service = retrofit.create(MoviesService.class);

        Call<Video_Path_API> call = service.getTrailerPath(obj.getId() + "");
        call.enqueue(new Callback<Video_Path_API>() {
            @Override
            public void onResponse(Call<Video_Path_API> call, Response<Video_Path_API> response) {
                Video_Path_API r = response.body();
                ArrayList<video> downloadedVideosPaths = r.getResults();
                tvvideos.addAll(downloadedVideosPaths);
                arr.addAll(tvvideos);
                loadimages();
                imagesVideosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Video_Path_API> call, Throwable t) {
//                Toast.makeText(TvDetailPage.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadimages() {

        Retrofit.Builder builder;
        builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        MoviesService service = retrofit.create(MoviesService.class);
        Call<ImagesRoot> call = service.getImagesPath(obj.getId() + "");
        call.enqueue(new Callback<ImagesRoot>() {
            @Override
            public void onResponse(Call<ImagesRoot> call, Response<ImagesRoot> response) {
                ImagesRoot r = response.body();
                ImagesPosition = arr.size();
                tvimages.addAll(r.getBackdrops());
                tvimages.addAll(r.getPosters());

                arr.addAll(tvimages);
                imagesVideosAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ImagesRoot> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(View view) {


    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public void GoHome(View view) {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);

    }
}
