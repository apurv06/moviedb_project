package com.example.apurv.flick.Adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.apurv.flick.Fragments.Movie.CastFragment;
import com.example.apurv.flick.Fragments.Movie.InfoFragment;
import com.example.apurv.flick.Fragments.Movie.ReviewsFragment;

public class DetailsViewPagerAdapter extends FragmentPagerAdapter {
    Bundle bundle;
    public DetailsViewPagerAdapter(FragmentManager fm,Bundle bundle) {
        super(fm);

        this.bundle=bundle;
    }

    @Override
    public Fragment getItem(int i) {

        Log.d("DetailsAdapter",i+"");
        if (i==0)
        {

            return InfoFragment.newInstance(bundle);
        }
        else if(i==1)
        {
            return CastFragment.newInstance(bundle);
        }
        else
        {
            return ReviewsFragment.newInstance(bundle);
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position) {

            int i=position;
        if (i == 0) {
            return "Info";
        } else if (i == 1) {
            return "Cast";
        } else {
            return "Reviews";
        }
    }
}
