package com.example.apurv.flick.Listeners;

import android.view.View;

import com.example.apurv.flick.CastAndCrew.Cast;
import com.example.apurv.flick.MoviesDirectory.movie;

public interface customCastClickListener {
    void onClickListener(View v, int position, Cast obj);
}
