
package com.example.apurv.flick.TvDirectory;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TvSeason {

    @SerializedName("air_date")
    private String mAirDate;
    @SerializedName("episodes")
    private List<Episode> mEpisodes;

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    String poster_path;


    public String getAirDate() {
        return mAirDate;
    }

    public void setAirDate(String airDate) {
        mAirDate = airDate;
    }

    public List<Episode> getEpisodes() {
        return mEpisodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        mEpisodes = episodes;
    }


}
