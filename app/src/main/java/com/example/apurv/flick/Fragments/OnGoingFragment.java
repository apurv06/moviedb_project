package com.example.apurv.flick.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.MediaType;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnGoingFragment extends Fragment {


    String backdrop;
    String poster;
    private String title;
    TextView genre;
    String Genre;
    MediaType obj;
    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public void setTitle(String title){this.title=title;}

    ImageView Backimage;
    ImageView Posterimage;
    public OnGoingFragment() {
        // Required empty public constructor
    }



Bundle bundle;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_viewpager, container, false);
        bundle = getArguments();
        if(bundle.getInt("type")==0)
        {
            obj=new Gson().fromJson(bundle.getString("Object"),TvShow.class);
        }
        else
        {
            obj=new Gson().fromJson(bundle.getString("Object"),movie.class);
        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bundle.getInt("type")==0)
                {
//                    Toast.makeText(getContext(), "Hello", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getContext(), TvDetailPage.class);
                    intent.putExtra("tvObject",new Gson().toJson(obj).toString());
                    startActivity(intent);
                }
                else
                {
//                    Toast.makeText(getContext(), "Hello", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getContext(), MovieDetailPage.class);
                    intent.putExtra("movieObject",new Gson().toJson(obj).toString());
                    startActivity(intent);
                }


            }
        });

        Backimage=view.findViewById(R.id.BackDrop);
        Posterimage=view.findViewById(R.id.Poster);
        genre=view.findViewById(R.id.genres);
        TextView titleView=view.findViewById(R.id.title);
        Uri uri=Uri.parse(BasePathHolder.posterPath+"w780"+"/"+backdrop);
        Picasso.get().load(uri).into(Backimage);

        uri=Uri.parse(BasePathHolder.posterPath+"w185"+"/"+poster);
        Picasso.get().load(uri).into(Posterimage);
        titleView.setText(title);
        genre.setText(Genre);

        return view;
    }

    public void setGenre(String genre) {
        this.Genre = genre;
    }
}
