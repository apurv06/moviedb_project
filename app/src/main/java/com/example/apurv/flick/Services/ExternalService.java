package com.example.apurv.flick.Services;

import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.TvDirectory.ExternIds;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ExternalService {

    @GET("{tv_id}/external_ids?api_key="+ ApiHolder.movieDbApi+"")
    Call<ExternIds> getExternalIds(@Path("tv_id")String tvId);

}
