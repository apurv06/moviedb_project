package com.example.apurv.flick.Fragments.Movie;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Movie;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.Fragments.Movie.OmdbInfo.Omdbinfo;
import com.example.apurv.flick.Images.Poster;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.Services.ExternalService;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.TvDirectory.ExternIds;
import com.example.apurv.flick.imdb.OmdbTv;
import com.google.gson.Gson;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment implements CustomItemListner, CustomItemLongClickListener {
    ArrayList<movie> similarmovieslist;
    ArrayList<movie> recommendationsmovieslist;
    Retrofit.Builder builder;
    Retrofit retrofit;
    RecyclerView similarMoviesView;
    RecyclerView recommendationsMoviesView;
    MovieAdapter similarmoviesAdapter;
    MovieAdapter recommendationsmoviesAdapter;

    TextView vote_average;
    TextView adult;
    TextView popularity;
    TextView release_date;
    TextView original_language;
    TextView original_title;
    ImageView Poster;
    ImageView Backdrop;
    ExpandableTextView overview;
    View view;

    Bundle bundle;
    movie obj;

    Button button;
    Omdbinfo omdbInfo;
    ExternIds externIds;


    MaterialCardView recomCard;
    MaterialCardView similarCard;
    private OnFragmentInteractionListener mListener;

    public InfoFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(Bundle bundle) {
        InfoFragment fragment = new InfoFragment();
        Bundle args =bundle;

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        bundle=getArguments();
        }
    }
    private void setBasicDetails() {



//        adult=view.findViewById(R.id.adult);
//        if(obj.getAdult())
//            adult.append("A");
//        else
//            adult.append("U/A");

//        popularity=view.findViewById(R.id.popularity);
//        popularity.append(obj.getPopularity()+"");

//        release_date=view.findViewById(R.id.release_date);
//        release_date.append(obj.getRelease_date());

//        original_language=view.findViewById(R.id.original_language);
//        original_language.append(obj.getOriginal_language());

        original_title=view.findViewById(R.id.original_title);
        if (obj.getOriginal_title()!=null)
        original_title.setText(obj.getOriginal_title());



//        Backdrop=view.findViewById(R.id.BackDrop);
//        uri=Uri.parse(BasePathHolder.posterPath+"w780"+"/"+ obj.getBackdrop_path());
//        Picasso.get().load(uri).into(Backdrop);

        ExpandableTextView plot = view.findViewById(R.id.sample2).findViewById(R.id.expand_text_view);
        if (omdbInfo.getPlot()!=null)
        plot.setText(omdbInfo.getPlot()+"");
        plot.setBackgroundColor(Color.BLACK);

        TextView runtime=view.findViewById(R.id.runtime);
        if (omdbInfo.getRuntime()!=null)
        runtime.append(omdbInfo.getRuntime()+"");

        release_date=view.findViewById(R.id.release_date);
        if (omdbInfo.getReleased()!=null)
        release_date.append(omdbInfo.getReleased());





        TextView genre=view.findViewById(R.id.genre);
        if (omdbInfo.getGenre()!=null)
        genre.append(omdbInfo.getGenre()+" ");




        Poster=view.findViewById(R.id.poster);
        if (omdbInfo.getPoster()!=null)
        {
            Uri uri = Uri.parse(omdbInfo.getPoster());
            Picasso.get().load(uri).into(Poster);
        }
        else
        {
            Poster.setBackground(getContext().getDrawable(R.drawable.noimage));
        }
        HashMap<String,String> map=new HashMap<>();
        if(omdbInfo.getRatings()!=null)
        for(int i=0;i<omdbInfo.getRatings().size();i++)
        {
            map.put(omdbInfo.getRatings().get(i).getSource(),omdbInfo.getRatings().get(i).getValue());
        }

        TextView imdb=view.findViewById(R.id.imdbscore);
        imdb.setText(" "+omdbInfo.getImdbRating());

        
        TextView moviedb=view.findViewById(R.id.moviedbscore);
        if(omdbInfo.getImdbVotes()!=null)
            moviedb.setText(" "+obj.getVote_average()+"/"+"10");
        else
            moviedb.setText(": N/A");
        
        TextView rottentomatoes=view.findViewById(R.id.rottenscore);
        if(map.containsKey("Rotten Tomatoes"))
            rottentomatoes.setText(" "+map.get("Rotten Tomatoes"));
        else
            rottentomatoes.setText(": N/A");


        TextView metacritic=view.findViewById(R.id.metacriticscore);
        if(map.containsKey("Metacritic"))
            metacritic.setText(" "+map.get("Metacritic"));
        else
            metacritic.setText(": N/A");

        overview= (ExpandableTextView) view.findViewById(R.id.sample1)
                .findViewById(R.id.expand_text_view);
        overview.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {
//                Toast.makeText(getActivity(), isExpanded ? "Expanded" : "Collapsed", Toast.LENGTH_SHORT).show();
            }
        });

        String info="";
        if (obj.getOverview()!=null)
          info="Overview\n"+obj.getOverview();

        if(omdbInfo.getActors()!=null)
            info=info+"\n\nActors: "+omdbInfo.getActors();

        if(omdbInfo.getDirector()!=null)
            info=info+"\n\nDirector: "+omdbInfo.getDirector();


        if(omdbInfo.getAwards()!=null)
            info=info+"\n\nAwards: "+omdbInfo.getAwards();


        if(omdbInfo.getBoxOffice()!=null)
            info=info+"\n\nBox Office: "+omdbInfo.getBoxOffice();

        if(omdbInfo.getWriter()!=null)
            info=info+"\n\nWriter: "+omdbInfo.getWriter();




        overview.setText(info);
        overview.setBackgroundColor(Color.BLACK);
    }

    private void loadMovies(final String category, final ArrayList<movie> list, final MovieAdapter movieAdapter) {
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<Movies_rootOfAPI> call= service.getSuggestedMovies(obj.getId()+"",category);
        call.enqueue(new Callback<Movies_rootOfAPI>() {
            @Override
            public void onResponse(Call<Movies_rootOfAPI> call, Response<Movies_rootOfAPI> response) {
                Movies_rootOfAPI r=response.body();
                ArrayList<movie> downloadedTopRatedLists=r.getResults();
                list.addAll(downloadedTopRatedLists);
                if(list.size()==0)
                {
                    if(category=="similar")
                    {
                        similarCard.setVisibility(View.GONE);
                    }
                    if(category=="recommendations")
                    {
                        recomCard.setVisibility(View.GONE);

                    }
                }
                movieAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<Movies_rootOfAPI> call, Throwable t) {

            }
        });
    }

    private void getExternIds() {
        Retrofit.Builder  builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        ExternalService service=retrofit.create(ExternalService.class);
        int id=obj.getId();
        Call<ExternIds> call= service.getExternalIds(obj.getId()+"");

        call.enqueue(new Callback<ExternIds>() {
            @Override
            public void onResponse(Call<ExternIds> call, Response<ExternIds> response) {
                externIds=response.body();
                FACEBOOK_PAGE_ID=externIds.getFacebookId();
                FACEBOOK_URL="https://www.facebook.com/"+externIds.getFacebookId();
                loadinfoFromOmdb();
            }

            @Override
            public void onFailure(Call<ExternIds> call, Throwable t) {
                Log.d("t1", t.getMessage());
            }
        });
    }
    private void loadinfoFromOmdb() {

        builder=new Retrofit.Builder().baseUrl("http://www.omdbapi.com/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        MoviesService services=retrofit.create(MoviesService.class);
        String id=externIds.getImdbId();
        Call<Omdbinfo> call=services.getInofromOmdb(externIds.getImdbId());
        call.enqueue(new Callback<Omdbinfo>() {
            @Override
            public void onResponse(Call<Omdbinfo> call, Response<Omdbinfo> response) {
                omdbInfo=response.body();
                setBasicDetails();
            }

            @Override
            public void onFailure(Call<Omdbinfo> call, Throwable t) {

            }
        });

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        view=inflater.inflate(R.layout.fragment_info, container, false);
        bundle = getArguments();
        obj=new Gson().fromJson(bundle.getString("movieObject"),movie.class);

        recomCard=view.findViewById(R.id.recommendCard);
        similarCard=view.findViewById(R.id.similarCard);


        button=view.findViewById(R.id.facebook);
        button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        openFacebookPage(view);
    }
});

        Button insta=view.findViewById(R.id.instagram);
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openInstagram(view);
            }
        });

        Button twitter=view.findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTwitter(view);
            }
        });
        similarmovieslist=new ArrayList<>();
        recommendationsmovieslist=new ArrayList<>();

        similarMoviesView=view.findViewById(R.id.similarMoviesList);
        similarMoviesView.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        similarmoviesAdapter=new MovieAdapter(getContext(),null,similarmovieslist,"w342", BasePathHolder.posterPathType,this,this,false);
        similarMoviesView.setAdapter(similarmoviesAdapter);

        recommendationsMoviesView=view.findViewById(R.id.recommendationsMoviesList);
        recommendationsMoviesView.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recommendationsmoviesAdapter=new MovieAdapter(getContext(),null,recommendationsmovieslist,"w342",BasePathHolder.posterPathType,this,this,false);
        recommendationsMoviesView.setAdapter(recommendationsmoviesAdapter);
        loadMovies("similar",similarmovieslist,similarmoviesAdapter);
        loadMovies("recommendations",recommendationsmovieslist,recommendationsmoviesAdapter);


        getExternIds();

        return view;
    }

    private void openTwitter(View view) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("twitter://user?screen_name="+externIds.getTwitterId()));
            startActivity(intent);
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/#!/"+externIds.getTwitterId())));
        }
    }

    public  String FACEBOOK_URL = "https://www.facebook.com/";
    public  String FACEBOOK_PAGE_ID = "";

    //method to get the right URL to use in the intent
    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    public void  openInstagram(View view)
    {
        Uri uri = Uri.parse("http://instagram.com/_u/"+externIds.getInstagramId());
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/"+externIds.getInstagramId())));
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void openFacebookPage(View view)
    {

        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(getContext());
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickListener(View v, int position, movie obj) {

        Intent intent = new Intent(getContext(), MovieDetailPage.class);
        intent.putExtra("movieObject", new Gson().toJson(obj).toString());
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(View v, int position, movie obj) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
