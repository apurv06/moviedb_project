package com.example.apurv.flick.Fragments.Movie;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apurv.flick.Adapters.DetailsViewPagerAdapter;
import com.example.apurv.flick.CastAndCrew.Cast;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.Listeners.customCastClickListener;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.google.gson.Gson;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieDetailsPageFragment extends Fragment implements customCastClickListener, CustomItemListner, CustomItemLongClickListener {



    movie obj;
    TabLayout tabLayout;
    ViewPager viewPager;
    DetailsViewPagerAdapter adapter;
    View view;
    public MovieDetailsPageFragment() {
        // Required empty public constructor
    }

    Bundle bundle;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bundle=getArguments();
        view=inflater.inflate(R.layout.fragment_movie_details_page, container, false);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager=view.findViewById(R.id.detailsViewPager);
        adapter=new DetailsViewPagerAdapter(getChildFragmentManager(),bundle);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        adapter.notifyDataSetChanged();
        tabLayout = view.findViewById(R.id.details_tab);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClickListener(View v, int position, Cast obj) {

    }

    @Override
    public void onClickListener(View v, int position, movie obj) {

    }

    @Override
    public void onItemLongClick(View v, int position, movie obj) {

    }


}
