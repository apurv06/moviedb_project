package com.example.apurv.flick.Fragments.People;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.PeopleCreditsAdapter;
import com.example.apurv.flick.Database.Movie;
import com.example.apurv.flick.Listeners.CreditClickListener;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.People.Credits.Cast;
import com.example.apurv.flick.People.Credits.Credit;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.Services.PeopleService;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PeopleMoviesFragment extends android.support.v4.app.Fragment implements CreditClickListener {
  Bundle bundle;
  View view;
  Long id;
  RecyclerView recyclerView;
  ArrayList<Cast> casts;
  PeopleCreditsAdapter adapter;
    private OnFragmentInteractionListener mListener;

    public PeopleMoviesFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PeopleMoviesFragment newInstance(Bundle bundle) {
        PeopleMoviesFragment fragment = new PeopleMoviesFragment();
        Bundle args =bundle;

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bundle=getArguments();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view=inflater.inflate(R.layout.fragment_people_movies, container, false);
        recyclerView=view.findViewById(R.id.movies);
        casts=new ArrayList<>();
       recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
       adapter=new PeopleCreditsAdapter(casts,this);
       recyclerView.setAdapter(adapter);
       id=bundle.getLong("id");

        loadmoviecredits();
       return  view;
    }

    private void loadmoviecredits() {
        Retrofit.Builder builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/person/").addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        PeopleService service=retrofit.create(PeopleService.class);
        Call<Credit> call=service.getMovieCredits(id+"");

        call.enqueue(new Callback<Credit>() {
            @Override
            public void onResponse(Call<Credit> call, Response<Credit> response) {
                casts.addAll(response.body().getCast());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Credit> call, Throwable t) {

            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void CreditClickListener(View v, int position) {
        Long id=casts.get(position).getId();
        Snackbar snackbar = Snackbar
                .make(view, "Loading Please Wait", Snackbar.LENGTH_LONG);

        snackbar.show();
        Retrofit.Builder builder=new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.themoviedb.org/3/movie/");

        Retrofit retrofit=builder.build();
        MoviesService services=retrofit.create(MoviesService.class);

        Call<movie> call=services.getMovie(id+"");
        call.enqueue(new Callback<movie>() {
            @Override
            public void onResponse(Call<movie> call, Response<movie> response) {
                movie obj=response.body();
                if(obj!=null) {

                    Intent intent = new Intent(getContext(), MovieDetailPage.class);
                    intent.putExtra("movieObject", new Gson().toJson(obj).toString());
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getContext(), "Data Currently Unavailable", Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<movie> call, Throwable t) {

            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
