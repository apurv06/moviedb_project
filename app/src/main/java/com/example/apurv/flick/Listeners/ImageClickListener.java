package com.example.apurv.flick.Listeners;

import android.view.View;

import com.example.apurv.flick.Images.Poster;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.People.Profile;

public interface ImageClickListener {

    void onClickListener(View v, int position, Profile obj);
}
