package com.example.apurv.flick.Adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.R;
import com.example.apurv.flick.TvDirectory.Episode;
import com.example.apurv.flick.TvDirectory.Season;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EpisodeRecycler extends RecyclerView.Adapter<EpisodeRecycler.EpisodeViewHolder> {


    private final ArrayList<Episode> list;

    public EpisodeRecycler(ArrayList<Episode> list) {
        this.list=list;
    }

    public class EpisodeViewHolder extends RecyclerView.ViewHolder
    {
        TextView episode;
        TextView name;
        TextView vote;
        ImageView imageView;
        public EpisodeViewHolder(@NonNull View itemView) {
            super(itemView);
            episode=itemView.findViewById(R.id.episode);
            vote=itemView.findViewById(R.id.airdate);
            imageView=itemView.findViewById(R.id.image);
            name=itemView.findViewById(R.id.name);
        }
    }


    @NonNull
    @Override
    public EpisodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        view=inflater.inflate(R.layout.episode,parent,false);
        final EpisodeRecycler.EpisodeViewHolder mViewHolder=new EpisodeRecycler.EpisodeViewHolder(view);

        return mViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull EpisodeViewHolder holder, int position) {

        String acs=list.get(position).getEpisodeNumber()+"";

        holder.name.setText("Episode "+list.get(position).getEpisodeNumber());
        holder.episode.setText(list.get(position).getName());
        holder.vote.setText("Rating: "+list.get(position).getVote_average());
        Uri uri=Uri.parse(BasePathHolder.posterPath+ PosterSizes.sizew342+"/"+ list.get(position).getStill_path());
        Picasso.get().load(uri).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
