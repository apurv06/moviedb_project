package com.example.apurv.flick.Listeners;

import android.view.View;

import com.example.apurv.flick.MoviesDirectory.movie;

public interface CustomItemLongClickListener {
    public void onItemLongClick(View v, int position,movie obj);
}
