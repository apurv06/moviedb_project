package com.example.apurv.flick.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apurv.flick.CastAndCrew.Cast;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.Listeners.customCastClickListener;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder> {
    Context context;
    ArrayList<Cast> list;
    String size;

    customCastClickListener clickListener;
    CustomItemLongClickListener longClickListener;

    public CastAdapter(Context context,ArrayList<Cast> list, String size, customCastClickListener clickListener) {
        this.context = context;
        this.list = list;
        this.size = size;

        this.clickListener = clickListener;

    }


    @NonNull
    @Override

    public CastAdapter.CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());

            view=inflater.inflate(R.layout.cast_layout,parent,false);

        final CastAdapter.CastViewHolder mViewHolder=new CastAdapter.CastViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClickListener(view,mViewHolder.getAdapterPosition(),list.get(mViewHolder.getAdapterPosition()));
            }
        });

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CastAdapter.CastViewHolder holder, int position) {

        String path=list.get(position).getProfilePath();
        holder.title.setText(list.get(position).getName());
        holder.role.setText("as "+list.get(position).getCharacter());
        Uri uri=Uri.parse(BasePathHolder.posterPath+size+"/"+ path);
        Picasso.get().load(uri).into(holder.CastPhoto);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CastViewHolder extends RecyclerView.ViewHolder{
        ImageView CastPhoto;
        TextView title;
        TextView role;
        public CastViewHolder(@NonNull View itemView) {
            super(itemView);
            CastPhoto=itemView.findViewById(R.id.poster);
            title=itemView.findViewById(R.id.title);
            role=itemView.findViewById(R.id.role);
            }
    }
}
