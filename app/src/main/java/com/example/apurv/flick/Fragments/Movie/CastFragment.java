package com.example.apurv.flick.Fragments.Movie;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apurv.flick.Adapters.CastAdapter;
import com.example.apurv.flick.CastAndCrew.Cast;
import com.example.apurv.flick.CastAndCrew.CastCrew;
import com.example.apurv.flick.Listeners.customCastClickListener;
import com.example.apurv.flick.People.PeopleDetailActivity;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CastFragment extends Fragment implements customCastClickListener {
    ArrayList<Cast> cast;
    Bundle bundle;
    movie obj;


    RecyclerView castView;

    CastAdapter castAdapter;
    View view;
    private OnFragmentInteractionListener mListener;

    public CastFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CastFragment newInstance(Bundle bundle) {
        CastFragment fragment = new CastFragment();
        Bundle args = bundle;

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        bundle = getArguments();
        obj=new Gson().fromJson(bundle.getString("movieObject"),movie.class);

        view=inflater.inflate(R.layout.fragment_cast, container, false);
        cast=new ArrayList<>();


        castView=view.findViewById(R.id.castList);
        castView.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL));
        castAdapter=new CastAdapter(getContext(),cast, PosterSizes.sizew185,this);
        castView.setAdapter(castAdapter);

        loadCastandCrew();
        return view;
    }

    private void loadCastandCrew() {
        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        MoviesService service=retrofit.create(MoviesService.class);
        Call<CastCrew> call= service.getCastAndCrew(obj.getId()+"");
        call.enqueue(new Callback<CastCrew>() {
            @Override
            public void onResponse(Call<CastCrew> call, Response<CastCrew> response) {
                CastCrew r=response.body();
                List<Cast> downloadedCast=r.getCast();
                cast.addAll(downloadedCast);

                castAdapter.notifyDataSetChanged();



            }

            @Override
            public void onFailure(Call<CastCrew> call, Throwable t) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickListener(View v, int position, Cast obj) {
        Intent intent=new Intent(getContext(), PeopleDetailActivity.class);
        intent.putExtra("id",obj.getId());
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
