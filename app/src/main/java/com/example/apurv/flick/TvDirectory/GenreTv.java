
package com.example.apurv.flick.TvDirectory;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GenreTv {

    @SerializedName("genres")
    private ArrayList<Genre> mGenres;

    public ArrayList<Genre> getGenres() {
        return mGenres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        mGenres = genres;
    }

}
