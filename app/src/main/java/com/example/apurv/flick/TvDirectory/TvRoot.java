
package com.example.apurv.flick.TvDirectory;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class TvRoot {

    @SerializedName("page")
    private Long mPage;
    @SerializedName("results")
    private ArrayList<TvShow> mTvShows;
    @SerializedName("total_pages")
    private Long mTotalPages;
    @SerializedName("total_results")
    private Long mTotalResults;

    public Long getPage() {
        return mPage;
    }

    public void setPage(Long page) {
        mPage = page;
    }

    public ArrayList<TvShow> getResults() {
        return mTvShows;
    }

    public void setResults(ArrayList<TvShow> tvShows) {
        mTvShows = tvShows;
    }

    public Long getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(Long totalPages) {
        mTotalPages = totalPages;
    }

    public Long getTotalResults() {
        return mTotalResults;
    }

    public void setTotalResults(Long totalResults) {
        mTotalResults = totalResults;
    }

}
