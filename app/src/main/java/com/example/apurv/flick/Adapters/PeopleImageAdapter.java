package com.example.apurv.flick.Adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Images.Poster;

import com.example.apurv.flick.Listeners.ImageClickListener;
import com.example.apurv.flick.People.Profile;
import com.example.apurv.flick.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PeopleImageAdapter extends RecyclerView.Adapter<PeopleImageAdapter.PeoplesViewHolder> {


    ArrayList<Profile> list;
    String size;
    int ImageType;
    ImageClickListener clickListener;
    Boolean showTitle;



    public PeopleImageAdapter(ArrayList<Profile> list, String size, int imageType, ImageClickListener clickListener) {

        this.list = list;

        this.size = size;
        this.showTitle=showTitle;
        ImageType = imageType;
        this.clickListener = clickListener;

    }


    @NonNull
    @Override

    public PeopleImageAdapter.PeoplesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());

                view = inflater.inflate(R.layout.suggested__items_layout, parent, false);

        final PeopleImageAdapter.PeoplesViewHolder mViewHolder=new PeopleImageAdapter.PeoplesViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickListener.onClickListener(view,mViewHolder.getAdapterPosition(),list.get(mViewHolder.getAdapterPosition()));
            }
        });

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PeoplesViewHolder holder, int position) {
        String path =  list.get(position).getFilePath();


        Uri uri=Uri.parse(BasePathHolder.posterPath+size+"/"+ path);
        Picasso.get().load(uri).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PeoplesViewHolder extends RecyclerView.ViewHolder{
        ImageView image;

        public PeoplesViewHolder(@NonNull View itemView) {
            super(itemView);
           image=itemView.findViewById(R.id.poster);
            }

        }
    }


