package com.example.apurv.flick.Fragments.Movie;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.ReviewsAdapter;


import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.ReviewService;

import com.example.apurv.flick.TvDirectory.Result;
import com.example.apurv.flick.TvDirectory.Reviewroot;
import com.google.gson.Gson;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ReviewsFragment extends Fragment {


    OnFragmentInteractionListener mListener;
    View view;
    RecyclerView recyclerView;
    ArrayList<Result> list;
     movie obj;
    Bundle bundle;
    ReviewsAdapter adapter;
    public ReviewsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static  ReviewsFragment newInstance(Bundle bundle) {
         ReviewsFragment fragment = new  ReviewsFragment();
        Bundle args = bundle;

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bundle=getArguments();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_tv_reviews, container, false);
        bundle = getArguments();
        obj=new Gson().fromJson(bundle.getString("movieObject"), movie.class);

        list=new ArrayList<>();


        recyclerView=view.findViewById(R.id.Reviews);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL));
        adapter = new ReviewsAdapter(list);
        recyclerView.setAdapter(adapter);
        fetchReviews();
        return  view;
    }

    private void fetchReviews() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create()).client(client);

        Retrofit retrofit = builder.build();
        ReviewService services = retrofit.create(ReviewService.class);
        Call<Reviewroot> call = services.getReviews(obj.getId() + "");

        call.enqueue(new Callback<Reviewroot>() {
            @Override
            public void onResponse(Call<Reviewroot> call, Response<Reviewroot> response) {

                list.addAll(response.body().getResults());
                if(list.size()==0)
                    Toast.makeText(getContext(), "No Reviews Available", Toast.LENGTH_SHORT).show();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Reviewroot> call, Throwable t) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
