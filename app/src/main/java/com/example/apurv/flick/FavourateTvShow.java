package com.example.apurv.flick;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.TvAdapter;
import com.example.apurv.flick.Adapters.TvAdapter;
import com.example.apurv.flick.Database.AppDatabase;
import com.example.apurv.flick.Database.Tv;
import com.example.apurv.flick.Database.TvDao;
import com.example.apurv.flick.Database.Tv;
import com.example.apurv.flick.Database.TvDao;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.Listeners.TvLongClick;
import com.example.apurv.flick.Listeners.TvshowCliclListener;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavourateTvShow extends AppCompatActivity implements TvLongClick {
    ArrayList<TvShow> list;
    RecyclerView recyclerView;
    TvAdapter adapter;
    AppDatabase mDb;
    TvDao mTvDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourate_tv_show);


        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Favourite Tvs");

        recyclerView=findViewById(R.id.recycler);
        list=new ArrayList<>();
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL));
        adapter=   new TvAdapter(this, list, null, "w342", BasePathHolder.posterPathType, new TvshowCliclListener() {
            @Override
            public void onClickListener(View v, int position, TvShow obj) {
                Intent intent = new Intent(getApplicationContext(), TvDetailPage.class);
                intent.putExtra("tvObject", new Gson().toJson(obj).toString());
                startActivity(intent);
            }
        }, this, false);

        recyclerView.setAdapter(adapter);

        mDb = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"database_1").allowMainThreadQueries().build();
        mTvDao = mDb.TvDao();// Get DAO object
        ArrayList<Tv> obj= (ArrayList<Tv>) mTvDao.getAll();
        if(obj==null||obj.size()==0)
        {
            Toast.makeText(this, "No Favourite Tv Shows Added", Toast.LENGTH_SHORT).show();
        }
        for (int i=0;i<obj.size();i++)
        {
            loadtv(obj.get(i).tvid);

        }
    }

    private void loadtv(Long id) {





        Retrofit.Builder builder=new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.themoviedb.org/3/tv/");

        Retrofit retrofit=builder.build();
        TvServices services=retrofit.create(TvServices.class);

        Call<TvShow> call=services.getTvShow(id+"");
        call.enqueue(new Callback<TvShow>() {
            @Override
            public void onResponse(Call<TvShow> call, Response<TvShow> response) {
                TvShow obj=response.body();
                list.add(obj);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TvShow> call, Throwable t) {

            }
        });
    }


    @Override
    public void onLongClickListener(View v,final int position,final TvShow obj) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FavourateTvShow.this);
        builder.setTitle("Confirm");
        builder.setMessage("Do you want to delete?");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        // Do something after 5s = 5000ms

                        Tv obj1=mTvDao.Search(obj.getId());

                        mTvDao.delete(obj1);
                        list.remove(position);
                        adapter.notifyDataSetChanged();

//                        long id = arr.get(pos).getId();
//                        String[] value = {Long.toString(id)};
//                        database.delete(Contract.Table_name, Contract.id + " = ? ", value);
//                        arr.remove(pos);
//                        adapter.notifyDataSetChanged();
//                        setListBack();

//                        Snackbar.make(getCurrentFocus(), "Want To Undo Change?", Snackbar.LENGTH_LONG)
//                                .setAction("UNDO", new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//
//                                        saveExpenseInDatabase(todoobj);
//                                        arr.add(position,todoobj);
//                                        adapter.notifyDataSetChanged();
//                                        return;
//                                    }
//                                })
//                                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
//                                .show();



                    }
                }
        );
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
}

