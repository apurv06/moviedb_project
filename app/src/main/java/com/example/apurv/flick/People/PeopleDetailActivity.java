package com.example.apurv.flick.People;

import android.app.TimePickerDialog;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.apurv.flick.Adapters.ImagePagerAdapter;
import com.example.apurv.flick.Adapters.ImagesVideosAdapter;
import com.example.apurv.flick.Adapters.PeopleImageAdapter;
import com.example.apurv.flick.Adapters.PeopleWorkViewAdapter;
import com.example.apurv.flick.Adapters.TvDetailsViewPagerAdapter;
import com.example.apurv.flick.Animations.ParallaxPageTransformer;
import com.example.apurv.flick.Animations.ViewPagerScroller;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Fragments.ImageViewPagerFragment;
import com.example.apurv.flick.Fragments.People.PeopleInfoFragment;
import com.example.apurv.flick.Fragments.People.PeopleMoviesFragment;
import com.example.apurv.flick.Fragments.People.PeopleTvFragment;
import com.example.apurv.flick.Images.ImagesRoot;
import com.example.apurv.flick.Images.Poster;
import com.example.apurv.flick.Listeners.ImageClickListener;
import com.example.apurv.flick.MoviesDirectory.Genre.Genre;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.MoviesService;
import com.example.apurv.flick.Services.PeopleService;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.apurv.flick.Animations.ParallaxPageTransformer.ParallaxTransformInformation.PARALLAX_EFFECT_DEFAULT;

public class PeopleDetailActivity extends AppCompatActivity implements PeopleInfoFragment.OnFragmentInteractionListener,ImageViewPagerFragment.OnFragmentInteractionListener,PeopleTvFragment.OnFragmentInteractionListener,PeopleMoviesFragment.OnFragmentInteractionListener, ImageClickListener {


    Long id;

    Toolbar toolbar;

    ViewPager works;
    PeopleWorkViewAdapter workAdapter;
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_detail);


        id = getIntent().getLongExtra("id", -1);

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        tabLayout=findViewById(R.id.works_tab);
        Bundle bundle=new Bundle();
        bundle.putLong("id",id);
        works= findViewById(R.id.works);
        works.setOffscreenPageLimit(3);
        workAdapter = new PeopleWorkViewAdapter(getSupportFragmentManager(),bundle );
        works.setAdapter(workAdapter);
        tabLayout.setupWithViewPager(works);


    }


    public void GoHome(View view) {
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onClickListener(View v, int position, Profile obj) {

    }
}
