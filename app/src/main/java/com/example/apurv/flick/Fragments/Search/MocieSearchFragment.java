package com.example.apurv.flick.Fragments.Search;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.Listeners.EndlessRecyclerViewScrollListener;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.Movies_rootOfAPI;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.Search;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MocieSearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MocieSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MocieSearchFragment extends Fragment implements CustomItemListner, CustomItemLongClickListener {


    private OnFragmentInteractionListener mListener;
    private String query;
    ArrayList<movie> list;
    RecyclerView rvItems;
    MovieAdapter adapter;
    View view;





    public MocieSearchFragment() {
        // Required empty public constructor
    }

    public void setQuery(String query)
    {
        this.query=query;
    }

    private void loadNextDataFromApi(int offset) {
      Retrofit.Builder  builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/search/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
       Search service = retrofit.create(Search.class);
        Call<Movies_rootOfAPI> call = service.getMovies(query,offset+"");

        call.enqueue(new Callback<Movies_rootOfAPI>() {
            @Override
            public void onResponse(Call<Movies_rootOfAPI> call, Response<Movies_rootOfAPI> response) {
                Movies_rootOfAPI r = response.body();
                ArrayList<movie> downloadedPopularLists = r.getResults();
                list.addAll(downloadedPopularLists);


                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Movies_rootOfAPI> call, Throwable t) {

            }
        });
    }


    public static android.support.v4.app.Fragment newInstance(String query) {
        MocieSearchFragment fragment = new MocieSearchFragment();
        Bundle args = new Bundle();
                       fragment.setQuery(query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
EndlessRecyclerViewScrollListener scrollListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view= inflater.inflate(R.layout.fragment_mocie_search, container, false);
        RecyclerView rvItems = (RecyclerView) view.findViewById(R.id.list);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        rvItems.setLayoutManager(layoutManager);

        list=new ArrayList<>();
        adapter=new MovieAdapter(getActivity(), null, list, "w342", BasePathHolder.posterPathType,this,this,false);

        rvItems.setAdapter(adapter);
        loadNextDataFromApi(1);
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                loadNextDataFromApi(page+1);
            }
        };
        // Adds the scroll listener to RecyclerView
        rvItems.addOnScrollListener(scrollListener);


    return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickListener(View v, int position, movie obj) {
        Intent intent = new Intent(getActivity(),MovieDetailPage.class);
        intent.putExtra("movieObject", new Gson().toJson(obj).toString());
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(View v, int position, movie obj) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
