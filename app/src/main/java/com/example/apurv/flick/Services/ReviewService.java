package com.example.apurv.flick.Services;

import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.TvDirectory.Reviewroot;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ReviewService {

    @GET("{tv_id}/reviews?api_key="+ ApiHolder.movieDbApi+"&language=en-US")
    Call<Reviewroot> getReviews(@Path("tv_id") String imdb_id);


}
