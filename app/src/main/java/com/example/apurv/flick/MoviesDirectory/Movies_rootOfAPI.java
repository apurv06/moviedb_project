package com.example.apurv.flick.MoviesDirectory;

import java.util.ArrayList;

public class Movies_rootOfAPI {
    int page;
    int total_results;
    int total_pages;
    ArrayList<movie> results;

    public int getPage() {
        return page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public ArrayList<movie> getResults() {
        return results;
    }
}
