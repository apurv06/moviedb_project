package com.example.apurv.flick.MoviesDirectory;

import com.example.apurv.flick.MoviesDirectory.video;

import java.util.ArrayList;

public class Video_Path_API {
    int id;
    ArrayList<video> results;

    public int getId() {
        return id;
    }

    public ArrayList<video> getResults() {
        return results;
    }
}
