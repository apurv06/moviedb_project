package com.example.apurv.flick;

public class PosterSizes {
    public static final String    sizew92="w92";
    public static final String    sizew154=  "w154";
    public static final String    sizew185=  "w185";
    public static final String    sizew342=  "w342";
    public static final String    sizew500=  "w500";
    public static final String    sizew780= "w780";
    public static final String    sizeoriginal= "original";
}
