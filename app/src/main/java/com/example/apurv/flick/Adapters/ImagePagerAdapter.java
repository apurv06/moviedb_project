package com.example.apurv.flick.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.apurv.flick.Fragments.ImageViewPagerFragment;
import com.example.apurv.flick.People.Result;

import java.util.ArrayList;

public class ImagePagerAdapter extends FragmentPagerAdapter {
    String path;
    ArrayList<Result> results;
    public ImagePagerAdapter(FragmentManager fm,ArrayList<Result> results) {
        super(fm);
       this.results=results;
    }

    @Override
    public Fragment getItem(int i) {
        ImageViewPagerFragment fragment=new ImageViewPagerFragment();
        fragment.setPoster(false);
        fragment.setPath(results.get(i).getFilePath());
        return fragment;
    }

    @Override
    public int getCount() {
        return results.size();
    }
}
