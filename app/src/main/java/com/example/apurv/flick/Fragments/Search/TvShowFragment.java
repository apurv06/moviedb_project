package com.example.apurv.flick.Fragments.Search;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.MovieAdapter;
import com.example.apurv.flick.Adapters.TvAdapter;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Listeners.EndlessRecyclerViewScrollListener;
import com.example.apurv.flick.Listeners.TvLongClick;
import com.example.apurv.flick.Listeners.TvshowCliclListener;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.Search;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.ShowAllActivity;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvRoot;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TvShowFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TvShowFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TvShowFragment extends Fragment implements TvshowCliclListener, TvLongClick {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

        OnFragmentInteractionListener mListener;
    private String query;
    ArrayList<TvShow> list;
    RecyclerView rvItems;
    TvAdapter adapter;
    View view;



    public TvShowFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TvShowFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static android.support.v4.app.Fragment newInstance(String param1, String param2) {
        TvShowFragment fragment = new TvShowFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    public void setQuery(String query)
    {
        this.query=query;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        query=mParam1;
        view= inflater.inflate(R.layout.fragment_tv_show, container, false);
        RecyclerView rvItems = (RecyclerView) view.findViewById(R.id.list);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        rvItems.setLayoutManager(layoutManager);

        list=new ArrayList<>();
        adapter=new TvAdapter(getActivity(),list, null,  "w342", BasePathHolder.posterPathType,this,this,false);

        rvItems.setAdapter(adapter);
        loadNextDataFromApi(1);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                loadNextDataFromApi(page+1);
            }
        };
        // Adds the scroll listener to RecyclerView
        rvItems.addOnScrollListener(scrollListener);

        return view;
    }

    private void loadNextDataFromApi(int offset) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/search/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        Search service = retrofit.create(Search.class);
        Call<TvRoot> call = service.getTv(query,offset+"");

        call.enqueue(new Callback<TvRoot>() {
            @Override
            public void onResponse(Call<TvRoot> call, Response<TvRoot> response) {
                TvRoot r = response.body();
                ArrayList<TvShow> downloadedPopularLists = r.getResults();
                list.addAll(downloadedPopularLists);


                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TvRoot> call, Throwable t) {

            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickListener(View v, int position, TvShow obj) {
        Intent intent = new Intent(getContext(),TvDetailPage.class);
        intent.putExtra("tvObject", new Gson().toJson(obj).toString());
        startActivity(intent);
    }

    @Override
    public void onLongClickListener(View v, int position, TvShow obj) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
