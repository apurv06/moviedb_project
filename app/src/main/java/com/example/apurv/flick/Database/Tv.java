package com.example.apurv.flick.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity
public class Tv {


    @PrimaryKey(autoGenerate = true)
    public int id;

    public Tv(Long tvid) {
        this.tvid = tvid;
    }


    public Long tvid;
}
