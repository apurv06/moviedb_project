package com.example.apurv.flick.Listeners;

import android.view.View;

import com.example.apurv.flick.TvDirectory.TvShow;

public interface TvLongClick {
    void onLongClickListener(View v, int position, TvShow obj);
}
