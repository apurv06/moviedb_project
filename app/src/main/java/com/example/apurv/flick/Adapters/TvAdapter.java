package com.example.apurv.flick.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apurv.flick.Listeners.TvLongClick;
import com.example.apurv.flick.TvDirectory.Genre;
import com.example.apurv.flick.Listeners.TvshowCliclListener;
import com.example.apurv.flick.BasePathHolder;

import com.example.apurv.flick.R;
import com.example.apurv.flick.TvDirectory.TvShow;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TvAdapter extends RecyclerView.Adapter<TvAdapter.TvViewHolder>{
    Context context;
    ArrayList<TvShow> list;
    String size;
    int ImageType;
    TvshowCliclListener clickListener;
    Boolean showTitle;
    TvLongClick longClickListener;
    ArrayList<Genre> tvGenre;
    public TvAdapter(Context context, ArrayList<TvShow> list, ArrayList<Genre> tvGenre, String size, int imageType, TvshowCliclListener clickListener, TvLongClick longClickListener, Boolean showTitle) {
        this.context = context;
        this.list = list;
        this.size = size;
        this.showTitle=showTitle;
        ImageType = imageType;
        this.clickListener = clickListener;
        this.longClickListener=longClickListener;
        this.tvGenre=tvGenre;
    }


    @NonNull
    @Override

    public TvAdapter.TvViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        if (ImageType== BasePathHolder.posterPathType) {
            if(showTitle)
                view = inflater.inflate(R.layout.home_layout_poster, parent, false);
            else
                view = inflater.inflate(R.layout.suggested__items_layout, parent, false);
        }
        else
            view=inflater.inflate(R.layout.home_layout_backdrop,parent,false);
        final TvAdapter.TvViewHolder mViewHolder=new TvAdapter.TvViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickListener.onClickListener(view,mViewHolder.getAdapterPosition(),list.get(mViewHolder.getAdapterPosition()));
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                    longClickListener.onLongClickListener(view,mViewHolder.getAdapterPosition(),list.get(mViewHolder.getAdapterPosition()));
                return true;
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TvAdapter.TvViewHolder holder, int position) {

        String path = ImageType == BasePathHolder.posterPathType ? list.get(position).getPosterPath() : list.get(position).getBackdropPath();

        if (showTitle) {
            if (tvGenre!=null)
            {
            list.get(position).setGenres(tvGenre);
            ArrayList<String> Genre = list.get(position).getGenres();
            for (int i = 0; i < Genre.size(); i++) {
                String genre = Genre.get(i);
                holder.genre.append(genre + " ");
            }
        }
        else
                holder.genre.setVisibility(View.GONE);
        holder.title.setText(list.get(position).getName());
    }

        Uri uri=Uri.parse(BasePathHolder.posterPath+size+"/"+ path);
        Picasso.get().load(uri).into(holder.Tvposter);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TvViewHolder extends RecyclerView.ViewHolder{
        ImageView Tvposter;
        TextView title;
        TextView genre;
        public TvViewHolder(@NonNull View itemView) {
            super(itemView);
            Tvposter=itemView.findViewById(R.id.poster);
            genre=itemView.findViewById(R.id.genres);
            if(showTitle)
                title=itemView.findViewById(R.id.title);
        }
    }
}
