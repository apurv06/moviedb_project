package com.example.apurv.flick.Fragments.People;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.ImagePagerAdapter;
import com.example.apurv.flick.Adapters.PeopleImageAdapter;
import com.example.apurv.flick.Animations.ParallaxPageTransformer;
import com.example.apurv.flick.Animations.ViewPagerScroller;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.Images.Poster;
import com.example.apurv.flick.Listeners.ImageClickListener;
import com.example.apurv.flick.People.Images;
import com.example.apurv.flick.People.PeopleClass;
import com.example.apurv.flick.People.PeopleDetailActivity;
import com.example.apurv.flick.People.Profile;
import com.example.apurv.flick.People.Result;
import com.example.apurv.flick.People.TaggedImages;
import com.example.apurv.flick.PosterSizes;
import com.example.apurv.flick.R;
import com.example.apurv.flick.Services.PeopleService;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.os.Build.VERSION_CODES.P;


public class PeopleInfoFragment extends Fragment implements ImageClickListener {
    PeopleClass obj;
    Bundle bundle;
    ViewPager viewPager;
    ImagePagerAdapter imagesAdapter;
    ArrayList<Result> arr;
    ImageView profile;
    Long id;
    TextView name;
    TextView origin;
    TextView birthday;
    ExpandableTextView moto;
    FrameLayout frameLayout;
    RecyclerView recyclerView;
    PeopleImageAdapter adapter;
    ArrayList<com.example.apurv.flick.People.Profile> Profile;

    View view;
    private OnFragmentInteractionListener mListener;

    public PeopleInfoFragment() {
        // Required empty public constructor
    }


    public static PeopleInfoFragment newInstance(Bundle bundle) {
        PeopleInfoFragment fragment = new PeopleInfoFragment();
        Bundle args =bundle;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bundle=getArguments();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        arr=new ArrayList<>();
        view=inflater.inflate(R.layout.fragment_people_info, container, false);
        bundle=getArguments();
        id=bundle.getLong("id",-1);

        frameLayout=view.findViewById(R.id.frame);
        Profile=new ArrayList<>();

        fetchpeopledetails();
        fetchtaggedimages();
        viewPager = view.findViewById(R.id.viewPager);
        imagesAdapter = new ImagePagerAdapter(getChildFragmentManager(), arr);
        viewPager.setAdapter(imagesAdapter);

        adapter=new PeopleImageAdapter(Profile, PosterSizes.sizew342, Poster.Type ,this);
        recyclerView=view.findViewById(R.id.images);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
        recyclerView.setAdapter(adapter);


        ParallaxPageTransformer pageTransformer = new ParallaxPageTransformer()
                .addViewToParallax(new ParallaxPageTransformer.ParallaxTransformInformation(R.id.BackDrop, 2, 2));
        viewPager.setPageTransformer(true, pageTransformer);

        Button button =view.findViewById(R.id.showImages);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setVisibility(View.VISIBLE);
            }
        });

        loadimages();
        changePagerScroller();
        return  view;
    }

    private void loadimages() {

        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/person/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        PeopleService service=retrofit.create(PeopleService.class);
        Call<Images> call= service.getImagesPath(id+"");
        call.enqueue(new Callback<Images>() {
            @Override
            public void onResponse(Call<Images> call, Response<Images> response) {
                Images r=response.body();
                Profile.addAll(r.getProfiles());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Images> call, Throwable t) {

            }
        });

    }

    private void changePagerScroller() {
        try {
            Field mScroller = null;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            ViewPagerScroller scroller = new ViewPagerScroller(viewPager.getContext());
            mScroller.set(viewPager, scroller);
        } catch (Exception e) {
            Log.e("tag4", "error of change scroller ", e);
        }
    }
    private void fetchtaggedimages() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/person/").addConverterFactory(GsonConverterFactory.create()).client(client);

        Retrofit retrofit = builder.build();
        PeopleService service = retrofit.create(PeopleService.class);
        Call<TaggedImages> call = service.getTaggedImages(id + "");
        Callback<TaggedImages> callback = new Callback<TaggedImages>() {
            @Override
            public void onResponse(Call<TaggedImages> call, Response<TaggedImages> response) {
                arr.addAll(response.body().getResults());
                imagesAdapter.notifyDataSetChanged();
                if(arr.size()==0) {
                   LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,60);

                    frameLayout.setLayoutParams(layoutParams);

                    viewPager.setVisibility(View.GONE);
                }
                int currentPage = 0;
                Timer timer;
                final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
                final long PERIOD_MS = 10000; // time in milliseconds between successive task executions.
                final int NUM_PAGES=arr.size();
                final Handler handler;
                handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (viewPager.getCurrentItem() == NUM_PAGES-1) {
                            viewPager.setCurrentItem(0);
                        }
                        viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true);
                    }
                };

                timer = new Timer(); // This will create a new Thread
                timer .schedule(new TimerTask() { // task to be scheduled

                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, DELAY_MS, PERIOD_MS);
            }

            @Override
            public void onFailure(Call<TaggedImages> call, Throwable t) {

            }
        };

        call.enqueue(callback);

    }

    private void fetchpeopledetails() {

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/person/").addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        PeopleService service = retrofit.create(PeopleService.class);
        Call<PeopleClass> call = service.getPeopleDetail(id + "");
        Callback<PeopleClass> callback = new Callback<PeopleClass>() {
            @Override
            public void onResponse(Call<PeopleClass> call, Response<PeopleClass> response) {
                Log.d("tag6",response.body().getName());
                obj=response.body();
                loadDetails();
                fetchtaggedimages();
            }

            @Override
            public void onFailure(Call<PeopleClass> call, Throwable t) {

            }
        };

        call.enqueue(callback);


    }

    private void loadDetails() {

        profile=view.findViewById(R.id.profile);
        Uri uri=Uri.parse(BasePathHolder.posterPath+ PosterSizes.sizew500+"/"+obj.getProfilePath());
        Picasso.get().load(uri).into(profile);

        name=view.findViewById(R.id.title);
        name.setText(obj.getName());

//        TextView popularity=view.findViewById(R.id.popularity);
//        popularity.append(obj.getPopularity()+"");

        TextView knownfor=view.findViewById(R.id.knownfor);
        knownfor.append(obj.getKnownForDepartment());

        origin=view.findViewById(R.id.birthplace);
        origin.setText(obj.getPlaceOfBirth()+"\n");

        birthday=view.findViewById(R.id.birthday);
        birthday.setText(""+obj.getBirthday());
        if (obj.getDeathday()==null)
        {
            if (obj.getBirthday()!=null) {
                String birth = obj.getBirthday();

                String year = birth.substring(0, 4);
                Calendar calendar = Calendar.getInstance();
                int current = calendar.get(Calendar.YEAR);
                birthday.append("," + (current - Integer.parseInt(year)));
            }
            else
            {
                birthday.setVisibility(View.GONE);
            }
            }
        else
        {
            birthday.append("  to  "+obj.getDeathday());
        }

        moto= (ExpandableTextView)view.findViewById(R.id.sample1)
                .findViewById(R.id.expand_text_view);
        moto.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {
//                Toast.makeText(getContext(), isExpanded ? "Expanded" : "Collapsed", Toast.LENGTH_SHORT).show();
            }
        });

        moto.setText(obj.getBiography());
        moto.setBackgroundColor(Color.BLACK);

        TextView popularity;
        popularity=view.findViewById(R.id.popularity);
        popularity.append(" "+obj.getPopularity());
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClickListener(View v, int position, Profile obj) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
