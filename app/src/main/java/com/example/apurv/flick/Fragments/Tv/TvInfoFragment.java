package com.example.apurv.flick.Fragments.Tv;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;


import android.support.design.card.MaterialCardView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apurv.flick.Adapters.TvAdapter;
import com.example.apurv.flick.Listeners.CustomItemListner;
import com.example.apurv.flick.Listeners.CustomItemLongClickListener;
import com.example.apurv.flick.Listeners.TvLongClick;
import com.example.apurv.flick.Listeners.TvshowCliclListener;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.MoviesDirectory.MovieDetailPage;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.Services.ExternalService;
import com.example.apurv.flick.Services.TvServices;
import com.example.apurv.flick.TvDirectory.ExternIds;
import com.example.apurv.flick.TvDirectory.Genre;
import com.example.apurv.flick.TvDirectory.GenreTv;
import com.example.apurv.flick.TvDirectory.TvDetailPage;
import com.example.apurv.flick.TvDirectory.TvRoot;
import com.example.apurv.flick.TvDirectory.TvShow;

import com.example.apurv.flick.R;
import com.example.apurv.flick.imdb.OmdbTv;
import com.google.gson.Gson;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TvInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TvInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TvInfoFragment extends Fragment implements  TvshowCliclListener, View.OnClickListener, TvLongClick {

    ArrayList<TvShow> similarTvslist;
    ArrayList<TvShow> recommendationsTvslist;
    Retrofit.Builder builder;
    Retrofit retrofit;
    RecyclerView similarTvsView;
    RecyclerView recommendationsTvsView;
    TvAdapter similarTvsAdapter;
    TvAdapter recommendationsTvsAdapter;

    TextView vote_average;
    TextView adult;
    TextView runtime;
    TextView genre;
    ExpandableTextView plot;
    TextView release_date;
    TextView original_language;
    TextView original_title;
    ImageView Poster;
    ImageView Backdrop;
    ExpandableTextView overview;
    View view;

    Bundle bundle;
    TvShow obj;

    OmdbTv omdbInfo;
    ExternIds externIds;

    MaterialCardView recomCard;
    MaterialCardView similarCard;

    ArrayList<Genre> genres;
    private OnFragmentInteractionListener mListener;

    public TvInfoFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TvInfoFragment newInstance(Bundle bundle) {
        TvInfoFragment fragment = new TvInfoFragment();
        Bundle args = bundle;

        fragment.setArguments(args);
        return fragment;
    }
    private void openTwitter(View view) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("twitter://user?screen_name="+externIds.getTwitterId()));
            startActivity(intent);
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/#!/"+externIds.getTwitterId())));
        }
    }
    public  String FACEBOOK_URL = "https://www.facebook.com/";
    public  String FACEBOOK_PAGE_ID = "";
    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }


    public void  openInstagram(View view)
    {
        Uri uri = Uri.parse("http://instagram.com/_u/"+externIds.getInstagramId());
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/"+externIds.getInstagramId())));
        }
    }

    public void openFacebookPage(View view)
    {

        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(getContext());
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);
    }
    private void setBasicDetails() {
//        vote_average=view.findViewById(R.id.vote_average);
//        vote_average.append(Double.toString(obj.getVoteAverage()));
//
//        double vote=obj.getVoteAverage();
//        if(vote<5)
//        {
//            vote_average.setBackgroundColor(Color.RED);
//        }
//        else if(vote<8)
//        {
//            vote_average.setBackgroundColor(Color.YELLOW);
//        }
//        else
//        {
//            vote_average.setBackgroundColor(Color.GREEN);
//        }



//        if(obj.get)
//            adult.append("A");
//        else
//            adult.append("U/A");

        HashMap<String,String> map=new HashMap<>();
        for(int i=0;i<omdbInfo.getRatings().size();i++)
        {
            map.put(omdbInfo.getRatings().get(i).getSource(),omdbInfo.getRatings().get(i).getValue());
        }

        TextView imdb=view.findViewById(R.id.imdbscore);
        imdb.setText(" "+omdbInfo.getImdbRating());


        TextView moviedb=view.findViewById(R.id.moviedbscore);
        if(omdbInfo.getImdbVotes()!=null)
            moviedb.setText(" "+obj.getVoteAverage()+"/"+"10");
        else
            moviedb.setText(": N/A");

        TextView rottentomatoes=view.findViewById(R.id.rottenscore);
        if(map.containsKey("Rotten Tomatoes"))
            rottentomatoes.setText(" "+map.get("Rotten Tomatoes"));
        else
            rottentomatoes.setText(": N/A");


        TextView metacritic=view.findViewById(R.id.metacriticscore);
        if(map.containsKey("Metacritic"))
            metacritic.setText(" "+map.get("Metacritic"));
        else
            metacritic.setText(": N/A");

        plot=view.findViewById(R.id.sample2).findViewById(R.id.expand_text_view);
        plot.setText(omdbInfo.getPlot()+"");
        plot.setBackgroundColor(Color.BLACK);

        runtime=view.findViewById(R.id.runtime);
        runtime.append(omdbInfo.getRuntime()+"");


        release_date=view.findViewById(R.id.release_date);
        if(omdbInfo.getReleased()!=null)
        release_date.append(omdbInfo.getReleased());
        else
            release_date.append("N/A");

        
        
        genre=view.findViewById(R.id.genre);
//        ArrayList<String> arr=obj.getGenres();
//        for (int i=0;i<arr.size();i++)
        genre.append(omdbInfo.getGenre()+" ");
        
//        original_language=view.findViewById(R.id.original_language);
//        original_language.append(obj.getOriginalLanguage());

        original_title=view.findViewById(R.id.original_title);
        original_title.append(obj.getName());

        Poster=view.findViewById(R.id.poster);
        Uri uri=Uri.parse(omdbInfo.getPoster());
        Picasso.get().load(uri).into(Poster);

        Backdrop=view.findViewById(R.id.BackDrop);
        uri=Uri.parse(BasePathHolder.posterPath+"w780"+"/"+ obj.getBackdropPath());
        Picasso.get().load(uri).into(Backdrop);

        overview= (ExpandableTextView) view.findViewById(R.id.sample1)
                .findViewById(R.id.expand_text_view);
        overview.setOnClickListener(this);
        overview.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {
//                Toast.makeText(getActivity(), isExpanded ? "Expanded" : "Collapsed", Toast.LENGTH_SHORT).show();
            }
        });

        overview.setText(obj.getOverview());
        overview.setBackgroundColor(Color.BLACK);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bundle=getArguments();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_tv_info, container, false);
        bundle = getArguments();
        obj=new Gson().fromJson(bundle.getString("tvObject"),TvShow.class);

        genres=new ArrayList<>();

        recomCard=view.findViewById(R.id.recommendCard);
        similarCard=view.findViewById(R.id.similarCard);

       Button button=view.findViewById(R.id.facebook);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFacebookPage(view);
            }
        });

        Button insta=view.findViewById(R.id.instagram);
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openInstagram(view);
            }
        });

        Button twitter=view.findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openTwitter(view);
            }
        });

        similarTvslist=new ArrayList<>();
        recommendationsTvslist=new ArrayList<>();

        similarTvsView=view.findViewById(R.id.similarTvsList);
        similarTvsView.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        similarTvsAdapter= new TvAdapter(getContext(),similarTvslist,null,"w342", BasePathHolder.posterPathType,this,this,false);
        similarTvsView.setAdapter(similarTvsAdapter);

        recommendationsTvsView=view.findViewById(R.id.recommendationsTvsList);
        recommendationsTvsView.setLayoutManager(new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.HORIZONTAL));
        recommendationsTvsAdapter=new TvAdapter(getContext(),recommendationsTvslist,null,"w342",BasePathHolder.posterPathType,this,this,false);
        recommendationsTvsView.setAdapter(recommendationsTvsAdapter);
        loadMovies("similar",similarTvslist,similarTvsAdapter);
        loadMovies("recommendations",recommendationsTvslist,recommendationsTvsAdapter);
        getExternIds();

    loadmoviegenre();

        return view;
    }

    private void loadmoviegenre() {

        Retrofit.Builder builder;
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/genre/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();
        TvServices service=retrofit.create(TvServices.class);
        Call<GenreTv> call= service.getTvGenre();
        call.enqueue(new Callback<GenreTv>() {
            @Override
            public void onResponse(Call<GenreTv> call, Response<GenreTv> response) {
                GenreTv r=response.body();
                ArrayList<com.example.apurv.flick.TvDirectory.Genre> downloadedGenre=r.getGenres();
                genres.addAll(downloadedGenre);
                obj.setGenres(genres);

            }

            @Override
            public void onFailure(Call<GenreTv> call, Throwable t) {

            }
        });

    }

    private void getExternIds() {
      Retrofit.Builder  builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        ExternalService service=retrofit.create(ExternalService.class);
        Long id=obj.getId();
        Call<ExternIds> call= service.getExternalIds(obj.getId()+"");

        call.enqueue(new Callback<ExternIds>() {
           @Override
           public void onResponse(Call<ExternIds> call, Response<ExternIds> response) {
               externIds=response.body();
               loadinfoFromOmdb();
           }

           @Override
           public void onFailure(Call<ExternIds> call, Throwable t) {
               Log.d("t1", t.getMessage());
           }
       });
    }

    private void loadinfoFromOmdb() {

        builder=new Retrofit.Builder().baseUrl("http://www.omdbapi.com/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        TvServices services=retrofit.create(TvServices.class);
        String id=externIds.getImdbId();
        Call<OmdbTv> call=services.getInofromOmdb(externIds.getImdbId());
        call.enqueue(new Callback<OmdbTv>() {
            @Override
            public void onResponse(Call<OmdbTv> call, Response<OmdbTv> response) {
                omdbInfo=response.body();
                setBasicDetails();
            }

            @Override
            public void onFailure(Call<OmdbTv> call, Throwable t) {

            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void loadMovies(final String category, final ArrayList<TvShow> list, final TvAdapter movieAdapter) {
        builder=new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/tv/")
                .addConverterFactory(GsonConverterFactory.create());

        retrofit=builder.build();
        TvServices service=retrofit.create(TvServices.class);
        Call<TvRoot> call= service.getSuggestedtvs(obj.getId()+"",category);
        call.enqueue(new Callback<TvRoot>() {
            @Override
            public void onResponse(Call<TvRoot> call, Response<TvRoot> response) {
                TvRoot r=response.body();
                ArrayList<TvShow> downloadedTopRatedLists=r.getResults();
                list.addAll(downloadedTopRatedLists);
                if(list.size()==0)
                {
                    if(category=="similar")
                    {
                       similarCard.setVisibility(View.GONE);

                    }
                    if(category=="recommendations")
                    {
                        recomCard.setVisibility(View.GONE);
                    }
                }
                movieAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(Call<TvRoot> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    @Override
    public void onClickListener(View v, int position, TvShow obj) {
        Intent intent = new Intent(getContext(), TvDetailPage.class);
        intent.putExtra("tvObject", new Gson().toJson(obj).toString());
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onLongClickListener(View v, int position, TvShow obj) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
