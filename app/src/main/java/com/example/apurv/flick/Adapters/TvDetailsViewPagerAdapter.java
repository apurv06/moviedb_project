package com.example.apurv.flick.Adapters;

import android.os.Bundle;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.example.apurv.flick.Fragments.Tv.TvCastFragment;
import com.example.apurv.flick.Fragments.Tv.TvInfoFragment;
import com.example.apurv.flick.Fragments.Tv.TvReviewsFragment;
import com.example.apurv.flick.Fragments.Tv.TvSeasons;

public class TvDetailsViewPagerAdapter extends FragmentPagerAdapter {

        Bundle bundle;
    public TvDetailsViewPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle=bundle;
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            return  TvInfoFragment.newInstance(bundle);
        } else if (i == 1) {

            return TvCastFragment.newInstance(bundle);
        } else if (i == 2) {
            return TvReviewsFragment.newInstance(bundle);
        } else
        {
            return TvSeasons.newInstance(bundle);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
int i=position;
        if (i == 0) {
            return  "Info";
        } else if (i == 1) {

            return "Cast";
        } else if (i == 2) {
            return "Reviews";
        } else
        {
            return "Seasons";
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
