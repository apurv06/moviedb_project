package com.example.apurv.flick.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = {Movie.class,Tv.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract MovieDao MovieDao();


    public static void destroyInstance() {
        INSTANCE = null;
    }

    public abstract TvDao TvDao();
}