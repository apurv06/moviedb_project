package com.example.apurv.flick.Adapters;


import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apurv.flick.Listeners.*;
import com.example.apurv.flick.BasePathHolder;
import com.example.apurv.flick.MoviesDirectory.Genre.Genre;
import com.example.apurv.flick.MoviesDirectory.movie;
import com.example.apurv.flick.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MoviesViewHolder> {

     Context context;
    ArrayList<movie> list;
    String size;
    int ImageType;
    CustomItemListner clickListener;
    Boolean showTitle;
    CustomItemLongClickListener longClickListener;
    ArrayList<Genre> moviesGenre;

    public MovieAdapter(Context context, ArrayList<Genre> moviesGenre, ArrayList<movie> list, String size, int imageType, CustomItemListner clickListener, CustomItemLongClickListener longClickListener, Boolean showTitle) {
        this.context = context;
        this.list = list;
        this.moviesGenre=moviesGenre;
        this.size = size;
        this.showTitle=showTitle;
        ImageType = imageType;
        this.clickListener = clickListener;
        this.longClickListener=longClickListener;

    }


    @NonNull
    @Override

    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        if (ImageType== BasePathHolder.posterPathType) {
            if(showTitle)
                view = inflater.inflate(R.layout.home_layout_poster, parent, false);
            else
            view = inflater.inflate(R.layout.suggested__items_layout, parent, false);
        }
        else
            view=inflater.inflate(R.layout.home_layout_backdrop,parent,false);
        final  MoviesViewHolder mViewHolder=new MoviesViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickListener.onClickListener(view,mViewHolder.getAdapterPosition(),list.get(mViewHolder.getAdapterPosition()));
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                longClickListener.onItemLongClick(view,mViewHolder.getAdapterPosition(),list.get(mViewHolder.getAdapterPosition()));
                return true;
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder holder, int position) {


        String path = ImageType == BasePathHolder.posterPathType ? list.get(position).getPoster_path() : list.get(position).getBackdrop_path();

        if (showTitle) {
            if (moviesGenre != null)
            {
                list.get(position).setGenres(moviesGenre);
                ArrayList<String> Genre = list.get(position).getGenres();
            for (int i = 0; i < Genre.size(); i++) {
                String genre = Genre.get(i);
                holder.genre.append(genre + " ");
            }
                        }
            else
                {
                    holder.genre.setVisibility(View.GONE);
                }
            holder.title.setText(list.get(position).getTitle());
        }

        Uri uri=Uri.parse(BasePathHolder.posterPath+size+"/"+ path);
        Picasso.get().load(uri).into(holder.movieposter);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder{
        ImageView movieposter;
        TextView title;
        TextView genre;
        public MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            movieposter=itemView.findViewById(R.id.poster);
            genre=itemView.findViewById(R.id.genres);
            if(showTitle) {
                title = itemView.findViewById(R.id.title);
            }

            }
    }


}
