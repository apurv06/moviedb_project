package com.example.apurv.flick.Listeners;

import android.widget.RadioGroup;

public interface CustomRadioListener extends RadioGroup.OnCheckedChangeListener{


    void onChanged(RadioGroup radioGroup, int i,Boolean auto);
}
