package com.example.apurv.flick.Services;


import com.example.apurv.flick.ApiHolder;
import com.example.apurv.flick.People.Images;
import com.example.apurv.flick.People.Credits.Credit;
import com.example.apurv.flick.People.PeopleClass;
import com.example.apurv.flick.People.TaggedImages;
import com.example.apurv.flick.TvDirectory.TvRoot;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PeopleService {

    @GET("{id}?api_key="+ ApiHolder.movieDbApi+"&language=en-US")
    Call<PeopleClass> getPeopleDetail(@Path("id")String id);

    @GET("{id}/tagged_images?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<TaggedImages> getTaggedImages(@Path("id")String id);

    @GET("{id}/movie_credits?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<Credit> getMovieCredits(@Path("id")String id);

    @GET("{id}/tv_credits?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<Credit> getTvCredits(@Path("id")String id);


    @GET("{id}/images?api_key="+ApiHolder.movieDbApi+"&language=en-US")
    Call<Images> getImagesPath(@Path("id") String s);
}
